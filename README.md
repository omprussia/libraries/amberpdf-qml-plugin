# AmberPDF QML plugin

This plugin provides `PdfView` QML type that can be used in the applications to display PDF documents. As a render backend, it depends on [AmberPDF](https://gitlab.com/omprusia/libraries/amberpdf) library.

The source code of the project is provided under
[the license](LICENSE.BSD-3-CLAUSE.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md)
documents the rights granted by contributors to the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules
of the Open Mobile Platform which informs you how we expect
the members of the community will interact while contributing and communicating.

For information about contributors see [AUTHORS](AUTHORS.md).

## Project Structure

The project has a common structure of a QML plugin based on C++ and QML for Aurora OS.

The sources are in the [plugin](plugin) directory.

The package files are in the [rpm](rpm) directory.

The files related to the documentation generator are in the [doc](doc) directory.
