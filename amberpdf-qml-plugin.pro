# SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = subdirs
SUBDIRS = \
    plugin

OTHER_FILES = \
    rpm/amberpdf-qml-plugin.spec
