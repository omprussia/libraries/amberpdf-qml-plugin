Name:       amberpdf-qml-plugin
Summary:    Qml plugin for AmberPDF
Version:    1.0.0
Release:    1
License:    BSD
URL:        https://developer.auroraos.ru/open-source
Source0:    %{name}-%{version}.tar.bz2
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  amberpdf-devel
BuildRequires:  pkgconfig(Qt5Concurrent)
BuildRequires:  qt5-qtdeclarative-devel-tools
BuildRequires:  qt5-plugin-platform-minimal

Requires:       sailfishsilica-qt5 >= 0.10.9
Requires:       amberpdf >= 1.0.0

%description
%{summary}.

%prep
%autosetup

%build
%qmake5
%make_build

%install
%make_install

%files
%{_libdir}/qt5/qml/ru/omp/amberpdf/*
