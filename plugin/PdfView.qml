// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.omp.amberpdf 1.0

Rectangle {
    id: root

    property alias count: pdfView.count
    property alias documentProvider: pdfView.documentProvider
    property alias currentIndex: pdfView.currentIndex
    property alias catchBound: pdfView.catchBound
    property alias orientation: pdfView.orientation
    property alias annotationsPaint: pdfView.annotationsPaint
    property alias notesPaint: pdfView.notesPaint
    property alias zoom: pdfView.itemScale
    property alias contentTopMargin: pdfView.contentTopMargin
    property alias pdfPath: pdfView.pdfPath
    property alias pageRange: pdfView.pageRange
    property alias status: pdfView.status
    property alias contentX: viewFlick.contentX
    property alias contentY: viewFlick.contentY
    property alias bookmarksModel: pdfView.bookmarksModel
    property alias fileVersion: pdfView.fileVersion
    property alias documentEdited: pdfView.documentEdited
    property alias pagesWithNotesModel: pdfView.pagesWithNotesModel
    property alias pageNumberComponent: pdfView.pageNumberComponent
    property alias grayScaleRendering: pdfView.grayScaleRendering
    property alias pageAspectRatio: pdfView.pageAspectRatio
    property alias reverse: pdfView.reverse
    property alias pageCheckComponent: pdfView.pageCheckComponent
    property alias hiddenPagesOpacity: pdfView.hiddenPagesOpacity
    property alias specialPagesCount: pdfView.specialPagesCount
    property alias pageSpacing: pdfView.pageSpacing
    property alias maxPageIndex: pdfView.maxPageIndex
    property alias minPageIndex: pdfView.minPageIndex
    property alias fitToPage: pdfView.fitToPage
    property alias centering: pdfView.centering

    signal clicked()
    signal clickedUrl(string url)
    signal noteActivate(string noteText, string author, int pageIndex)
    signal contentChanged()
    signal documentSaved(bool saveStatus)
    signal holding(var lastMouse, var screenCoordinates)
    signal noteRemoved(int pageIndex, int noteId, bool result)

    function correctPosition() {
        viewFlick.contentY = pdfView.contentY
        viewFlick.contentX = pdfView.contentX

        viewFlick.returnToBounds()
    }
    function goToPage(pageIndex) {
        viewPositionBlocker.start()
        pdfView.positionViewAtIndex(pageIndex)
    }
    function saveDocumentAs(path) {
        pdfView.saveDocumentAs(path)
    }
    function addAnnotation(rect, author, content, color) {
        rect.x += viewFlick.contentX
        rect.y += viewFlick.contentY
        pdfView.addAnnotation(rect, color, author, content)
    }
    function removeNote(pageIndex, noteIndex) {
        pdfView.removeNote(pageIndex, noteIndex)
    }
    function editNote(pageIndex, noteIndex, newColor, newContent) {
        pdfView.editNote(pageIndex, noteIndex, newContent, newColor)
    }

    color: "transparent"
    data: [
        PdfViewPrivate {
            id: pdfView

            parent: viewFlick
            height: parent.height
            width: parent.width
            anchors.top: parent.top

            onWidthChanged: {
                viewPositionBlocker.start()
                root.correctPosition()
            }
            onHeightChanged: {
                viewPositionBlocker.start()
                root.correctPosition()
            }
            onContentChanged: {
                root.correctPosition()
                root.contentChanged()
            }
            onOrientationChanged: root.correctPosition()
            onItemScaleChanged: root.correctPosition()
            onClickedUrl: root.clickedUrl(url)
            onClickedGoToPage: root.correctPosition()
            onNoteActivated: root.noteActivate(noteText, author, pageIndex)
            onDocumentSaved: root.documentSaved(saveStatus)
            onNoteRemoved: root.noteRemoved(pageIndex, noteId, result)
        },
        SilicaFlickable {
            id: viewFlick

            property var fixedScaleValues: [0.5, 1.0, 1.5, 2]
            property real maximumItemScale: fixedScaleValues[fixedScaleValues.length - 1]
            property real minimumItemScale: fixedScaleValues[0]
            property real maximumPinchScale: 6.0
            property real storedRootContentPosition: 0

            function scaleAroundPoint(center, newScale) {
                pdfView.scaleAroundPoint(center, newScale)
                root.correctPosition()
            }
            function clicked(mouse) {
                var screenX = mouse.x - contentX
                var screenY = mouse.y - contentY

                if (pdfView.pageCheckComponent)
                    pdfView.clicked(Qt.point(mouse.x, mouse.y))

                root.clicked()
            }
            function doubleClicked(mouse) {
                var currentScale = fixedScaleValues.indexOf(Math.round(pdfView.itemScale * 100) / 100)

                if (currentScale === -1) {
                    scaleAroundPoint(Qt.point(mouse.x, mouse.y), 1.0)
                    return
                }

                var newScale = fixedScaleValues[Math.max(1, (++currentScale) % fixedScaleValues.length)]
                scaleAroundPoint(Qt.point(mouse.x, mouse.y), newScale)
            }

            contentHeight: Math.max(height, (pdfView.orientation === Qt.Vertical
                                    ? pdfView.contentHeight * pdfView.itemScale + pdfView.lastPageIndent
                                    : pdfView.contentHeight))
            contentWidth: Math.max(width, (pdfView.orientation === Qt.Vertical
                                   ? pdfView.contentWidth
                                   : pdfView.contentWidth * pdfView.itemScale + pdfView.lastPageIndent))
            flickableDirection: Flickable.AutoFlickDirection
            anchors.fill: parent

            onContentYChanged: {
                if (viewPositionBlocker.running)
                    return

                pdfView.contentY = contentY
            }
            onContentXChanged: {
                if (viewPositionBlocker.running)
                    return

                pdfView.contentX = contentX
            }
            onQuickScrollAnimatingChanged: {
                if (quickScrollAnimating) {
                    switch (root.orientation) {
                    case Qt.Horizontal:
                        viewFlick.storedRootContentPosition = contentX
                        break
                    case Qt.Vertical:
                        viewFlick.storedRootContentPosition = contentY
                        break
                    }
                } else {
                    switch (root.orientation) {
                    case Qt.Horizontal:
                        if (contentX < viewFlick.storedRootContentPosition) {
                            root.goToPage(0)
                        } else if (contentX > viewFlick.storedRootContentPosition) {
                            root.goToPage(root.count - 1)
                        }
                        break
                    case Qt.Vertical:
                        if (contentY < viewFlick.storedRootContentPosition) {
                            root.goToPage(0)
                        } else if (contentY > viewFlick.storedRootContentPosition) {
                            root.goToPage(root.count - 1)
                        }
                        break
                    }
                }
            }

            Timer {
                id: viewPositionBlocker

                interval: 100
                repeat: false

                onTriggered: root.correctPosition()
            }

            ScrollDecorator {  }

            PinchArea {
                id: pinchArea

                property real startScale
                property bool moving: false

                anchors.fill: parent
                enabled: pdfView.status === PdfDocument.Ready

                onMovingChanged: if (moving) root.moving()
                onPinchStarted: {
                    if (viewFlick.moving)
                        viewFlick.cancelFlick()

                    startScale = pdfView.itemScale
                }
                onPinchUpdated: {
                    viewFlick.scaleAroundPoint(pinch.center,
                                               Math.max(viewFlick.minimumItemScale,
                                                        Math.min(viewFlick.maximumPinchScale, startScale * pinch.scale)))
                }
                onPinchFinished: viewFlick.returnToBounds()

                MouseArea {
                    id: mouseArea

                    anchors.fill: parent
                    enabled: parent.enabled
                    property point lastMouse
                    property double deltaX: 10.0
                    property double deltaY: 10.0
                    property int doubleClickLatency: 350
                    property int holdLatency: 800

                    signal click(var lastMouse)
                    signal doubleClick(var lastMouse)
                    signal hold(var lastMouse, var screenCoordinates)

                    onClick: viewFlick.clicked(lastMouse)
                    onDoubleClick: viewFlick.doubleClicked(lastMouse)
                    onHold: root.holding(lastMouse, screenCoordinates)
                    onPressed: {
                        lastMouse = Qt.point(mouseX, mouseY)
                        holdTimer.start()
                    }
                    onReleased: {
                        lastMouse = Qt.point(mouseX, mouseY)

                        if (doubleClickTimer.running) {
                            doubleClickTimer.stop()
                            doubleClick(lastMouse)

                            if (holdTimer.running)
                                holdTimer.stop()

                            return
                        }

                        if (holdTimer.running) {
                            holdTimer.stop()
                            doubleClickTimer.start()
                        }
                    }
                    onMouseXChanged: {
                        if (!holdTimer.running)
                            return

                        if (Math.abs(mouseX - lastMouse.x) > deltaX)
                            holdTimer.stop()
                    }
                    onMouseYChanged: {
                        if (!holdTimer.running)
                            return

                        if (Math.abs(mouseY - lastMouse.y) > deltaY)
                            holdTimer.stop()
                    }

                    Timer {
                        id: doubleClickTimer

                        property var lastMouse

                        interval: mouseArea.doubleClickLatency

                        onTriggered: mouseArea.click(mouseArea.lastMouse)
                    }
                    Timer {
                        id: holdTimer

                        interval: mouseArea.holdLatency
                        repeat: false

                        onTriggered: {
                            if (doubleClickTimer.running)
                                doubleClickTimer.stop()

                            mouseArea.hold(mouseArea.lastMouse,
                                           Qt.point(mouseArea.lastMouse.x - viewFlick.contentX,
                                                    mouseArea.lastMouse.y - viewFlick.contentY))
                        }
                    }
                }
            }
        }
    ]
}
