# SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = lib
QT += qml quick
QT -= gui
CONFIG += qt plugin hide_symbols c++17 link_pkgconfig

LIBS += -lamberpdf

TARGET = $$qtLibraryTarget(amberpdfqmlplugin)
target.path = $$[QT_INSTALL_QML]/ru/omp/amberpdf

qml.files = qmldir *.qml plugins.qmltypes
qml.path = $$[QT_INSTALL_QML]/ru/omp/amberpdf

INSTALLS = target qml

VIEW_HEADERS += \
    src/interface/basedocument.h \
    src/interface/basepage.h \
    src/bitmaploadworker.h \
    src/pagebitmaphelper.h \
    src/pdfpagetile.h \
    src/pdfviewtypes.h \
    src/pdfpagecontainer.h \
    src/documentmapper.h \
    src/pdfview.h \
    src/interface/baseannotation.h \
    src/pdfsimpleannotation.h \
    src/pdfsimplenote.h \
    src/bookmarksmodel.h \
    src/interface/basebookmark.h \
    src/notesmodel.h \
    src/pageswithnotesmodel.h \
    src/pagewithnotessortmodel.h \
    src/pdfbackgroundpage.h \

PROVIDER_HEADERS += \
    src/pdfium-provider/pagepreloader.h \
    src/pdfium-provider/pagessizesloader.h \
    src/pdfium-provider/pdfdocumentitem.h \
    src/pdfium-provider/pdfpageitem.h \

VIEW_SOURCES += \
    src/interface/basedocument.cpp \
    src/interface/basepage.cpp \
    src/bitmaploadworker.cpp \
    src/pagebitmaphelper.cpp \
    src/pdfpagetile.cpp \
    src/pdfpagecontainer.cpp \
    src/documentmapper.cpp \
    src/pdfview.cpp \
    src/interface/baseannotation.cpp \
    src/pdfsimpleannotation.cpp \
    src/pdfsimplenote.cpp \
    src/bookmarksmodel.cpp \
    src/notesmodel.cpp \
    src/pageswithnotesmodel.cpp \
    src/pagewithnotessortmodel.cpp \
    src/pdfbackgroundpage.cpp \

PROVIDER_SOURCES += \
    src/pdfium-provider/pagepreloader.cpp \
    src/pdfium-provider/pagessizesloader.cpp \
    src/pdfium-provider/pdfdocumentitem.cpp \
    src/pdfium-provider/pdfpageitem.cpp \

HEADERS += \
    $$PROVIDER_HEADERS \
    $$VIEW_HEADERS \

SOURCES += \
    $$PROVIDER_SOURCES \
    $$VIEW_SOURCES \
    src/plugin.cpp \

INCLUDEPATH += \
    src \
    src/interface \
    src/pdfium-provider \

DISTFILES += *.qml

DEFINES += \
    PRELOAD_PAGES_COUNT=10 \
    HIGHLIGHT_OPACITY_ANNOT_ON=178 \
    HIGHLIGHT_OPACITY_ANNOT_OFF=42 \
    HIGHLIGHT_OPACITY_NOTE_ON=198 \
    HIGHLIGHT_OPACITY_NOTE_OFF=100 \

qmltypes.commands = qmlplugindump -nonrelocatable ru.omp.amberpdf 1.0 > $$PWD/plugins.qmltypes
QMAKE_EXTRA_TARGETS += qmltypes
