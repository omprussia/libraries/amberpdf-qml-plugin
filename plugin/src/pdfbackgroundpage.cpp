// SPDX-FileCopyrightText: 2023-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QTimer>
#include <QSGTexture>
#include <QSGSimpleTextureNode>
#include <QPropertyAnimation>
#include <QQuickWindow>
#include <QThreadPool>
#include <QtMath>
#include <QPainter>

#include "basepage.h"
#include "bitmaploadworker.h"
#include "pagebitmaphelper.h"

#include "pdfbackgroundpage.h"

PdfBackgroundPage::PdfBackgroundPage(QQuickItem *parent)
    : QQuickItem(parent)
    , m_needUpdateImage(false)
    , m_needClearImage(false)
    , m_renderInProcess(false)
    , m_renderable(false)
    , m_forceRender(false)
    , m_imageScale(1.0)
    , m_pageScale(1.0)
    , m_centering(false)
    , m_fitToPage(true)
{
    setFlag(QQuickItem::ItemHasContents, true);

    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &PdfBackgroundPage::_loadBitmap);

    m_timer->setInterval(150);
    m_timer->setSingleShot(true);

    m_timerClear = new QTimer(this);
    connect(m_timerClear, &QTimer::timeout, this, &PdfBackgroundPage::_clearImage);

    m_timerClear->setInterval(5000);
    m_timerClear->setSingleShot(true);

    connect(this, &PdfBackgroundPage::renderableChanged, m_timer, static_cast<void(QTimer::*)(void)>(&QTimer::start));
    connect(this, &PdfBackgroundPage::bitmapError, m_timer, static_cast<void(QTimer::*)(void)>(&QTimer::start));
    connect(this, &PdfBackgroundPage::partReady, this, &PdfBackgroundPage::update);

    m_animation = new QPropertyAnimation(this, "opacity");
    m_animation->setDuration(180);
    m_animation->setStartValue(0.42);
    m_animation->setEndValue(1.0);

    connect(m_animation, &QPropertyAnimation::finished, this, &PdfBackgroundPage::animationEnded);
}

PdfBackgroundPage::~PdfBackgroundPage()
{
    m_timer->stop();
    m_timerClear->stop();
}

QSGNode *PdfBackgroundPage::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    if (height() <= 0 || width() <= 0)
        return nullptr;

    auto node = static_cast<QSGSimpleTextureNode *>(oldNode);

    if (node == nullptr) {
        node = new QSGSimpleTextureNode();
        node->setOwnsTexture(true);
    }

    if (node == nullptr)
        qFatal("Error: create node error.");

    if (m_needClearImage && node->texture()) {
        node->texture()->deleteLater();
        auto paper = QImage(1, 1, QImage::Format_RGBA8888);
        paper.fill(Qt::white);
        node->setTexture(window()->createTextureFromImage(paper, QQuickWindow::TextureHasAlphaChannel));
        m_needClearImage = false;
    }

    if (node->texture() == nullptr) {
        auto paper = QImage(1, 1, QImage::Format_RGBA8888);
        paper.fill(Qt::white);
        node->setTexture(window()->createTextureFromImage(paper, QQuickWindow::TextureHasAlphaChannel));

        if (!m_pagePart.isNull())
            m_needUpdateImage = true;
    }

    if (m_renderable && m_needUpdateImage) {
        if (m_pagePart.isNull()) {
            emit bitmapError();
        } else {
            node->texture()->deleteLater();
            node->setTexture(window()->createTextureFromImage(m_pagePart, QQuickWindow::TextureCanUseAtlas));
        }

        m_needUpdateImage = false;
    }

    if (m_renderable && !m_renderInProcess && m_pagePart.isNull())
        emit bitmapError();

    node->setRect(boundingRect());

    return node;
}

void PdfBackgroundPage::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickItem::geometryChanged(newGeometry, oldGeometry);
}

void PdfBackgroundPage::setPageSource(QSharedPointer<BasePage> pageSource)
{
    if (!pageSource)
        return;

    m_pageSource = pageSource;
    m_timer->start();
}

void PdfBackgroundPage::setActualPageSize(const QSizeF &actualPageSize)
{
    if (m_actualPageSize == actualPageSize)
        return;

    m_actualPageSize = actualPageSize;
}

bool PdfBackgroundPage::renderable() const
{
    return m_renderable;
}

bool PdfBackgroundPage::isBitmap() const
{
    return !m_pagePart.isNull();
}

void PdfBackgroundPage::setImageScale(qreal imageScale)
{
    if (qFuzzyCompare(imageScale, m_imageScale))
        return;

    m_imageScale = imageScale;
}

void PdfBackgroundPage::setPageScale(qreal pageScale)
{
    if (qFuzzyCompare(pageScale, m_pageScale))
        return;

    m_pageScale = pageScale;
}

void PdfBackgroundPage::render(bool force)
{
    m_forceRender = force;
    m_timer->start();
}

void PdfBackgroundPage::setRenderable(bool renderable)
{
    if (m_renderable == renderable)
        return;

    m_renderable = renderable;
    emit renderableChanged(m_renderable);

    if (!m_renderable)
        QMetaObject::invokeMethod(m_timerClear, "start");
    else
        QMetaObject::invokeMethod(m_timerClear, "stop");
}

void PdfBackgroundPage::setCentering(bool centering)
{
    if (m_centering == centering)
        return;

    m_centering = centering;
}

void PdfBackgroundPage::setFitToPage(bool fitToPage)
{
    if (m_fitToPage == fitToPage)
        return;

    m_fitToPage = fitToPage;
}

void PdfBackgroundPage::_loadBitmap()
{
    if (!m_pageSource)
        return;

    if (!m_renderable || m_renderInProcess) {
        if (m_loader)
            m_loader->cancel();

        return;
    }

    if (width() <= 0 || height() <= 0)
        return;

    auto pageSize = m_pageSource->originalSize();
    if (!pageSize.isFinished() || pageSize.isCanceled()) {
        m_timer->start();
        return;
    }

    auto pageScaleX = width() / pageSize.result().width() / m_pageScale;
    auto pageScaleY = height() / pageSize.result().height() / m_pageScale;

    if (!m_forceRender && qFloor(width()) == m_pagePart.width() && qFloor(height()) == m_pagePart.height())
        return;

    m_loader = new BitmapLoaderWorker(m_pageSource,
                                      pageScaleX,
                                      pageScaleY,
                                      0,
                                      m_imageScale / m_pageScale,
                                      bitmaphelper::bias(QPointF(-x(), -y()),
                                                         QSizeF(pageSize.result().width(),
                                                                pageSize.result().height()),
                                                         m_actualPageSize,
                                                         m_imageScale,
                                                         m_centering,
                                                         m_fitToPage));
    connect(this, &PdfBackgroundPage::destroyed, m_loader.data(), &BitmapLoaderWorker::cancel);
    connect(this, &PdfBackgroundPage::stopRender, m_loader.data(), &BitmapLoaderWorker::cancel, Qt::DirectConnection);
    connect(m_loader, &BitmapLoaderWorker::done, this, [this](QImage result) {
        if (m_loader != sender())
            return;

        m_renderInProcess = false;

        if (result.isNull()) {
            emit bitmapError();
        } else {
            auto needAnimation = m_pagePart.isNull();
            m_pagePart = result;
            m_needUpdateImage = true;
            emit partReady();

            if (needAnimation) {
                m_animation->setStartValue(m_pagePart.isNull() ? 0.42 : 0.95);
                m_animation->start();
            }
            update();
        }
    });

    QThreadPool::globalInstance()->start(m_loader);
    m_renderInProcess = true;
    m_forceRender = false;

}

void PdfBackgroundPage::_clearImage()
{
    if (!m_renderable) {
        m_pagePart = QImage();
        m_needClearImage = true;
        update();
    }
}
