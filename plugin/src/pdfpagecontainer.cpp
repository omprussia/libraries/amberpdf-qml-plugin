// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "QQuickWindow"
#include <QSGSimpleTextureNode>
#include <QtMath>
#include <QFutureWatcher>

#include "pdfpagetile.h"
#include "pdfsimpleannotation.h"
#include "pdfsimplenote.h"
#include "baseannotation.h"
#include "basepage.h"
#include "pdfbackgroundpage.h"

#include "pdfpagecontainer.h"

#include <amberpdf/pdfpage.h>

PdfPageContainer::PdfPageContainer(QQuickItem *parent)
    : QQuickItem(parent)
    , m_scale(1.0)
    , m_mapper(nullptr)
    , m_backgroundPage(nullptr)
    , m_maxTileZ(0)
    , m_tileSize(256)
    , m_annotationsPaint(false)
    , m_notesPaint(false)
    , m_allTilesReady(false)
    , m_grayScaleRendering(false)
    , m_checked(false)
    , m_pageIndex(-1)
    , m_centering(false)
    , m_fitToPage(true)
{
    setFlag(QQuickItem::ItemHasContents, true);

    auto itemWindow = window();
    if (itemWindow != nullptr)
        connect(itemWindow, &QQuickWindow::beforeSynchronizing, this, &PdfPageContainer::_updateVisible, Qt::DirectConnection);
}

PdfPageContainer::~PdfPageContainer() = default;

QSGNode *PdfPageContainer::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    return oldNode;
}

void PdfPageContainer::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickItem::geometryChanged(newGeometry, oldGeometry);

    if (qFuzzyCompare(newGeometry.width(), oldGeometry.width()) && qFuzzyCompare(newGeometry.height(), oldGeometry.height()))
        return;

    auto pageRate = width() / m_pageGeometry.width();

    if (m_backgroundPage) {
        m_backgroundPage->setWidth(width());
        m_backgroundPage->setHeight(height());
        m_backgroundPage->setImageScale(pageRate);
        m_backgroundPage->setPageScale(m_scale);
    }

    QMutableListIterator<PdfSimpleAnnotation *> annotIter(m_annotationsItems);
    while (annotIter.hasNext()) {
        annotIter.next();
        if (annotIter.value() == nullptr) {
            annotIter.remove();
            continue;
        }
        auto pdfAnnotation = annotIter.value()->source();
        annotIter.value()->setX(pdfAnnotation->rect.x() * pageRate);
        annotIter.value()->setY((m_pageGeometry.height() - pdfAnnotation->rect.y()) * pageRate - pdfAnnotation->rect.height() * pageRate);
        annotIter.value()->setWidth(pdfAnnotation->rect.width() * pageRate);
        annotIter.value()->setHeight(pdfAnnotation->rect.height() * pageRate);

    }

    QMutableListIterator<PdfSimpleNote *> noteIter(m_notesItems);
    while (noteIter.hasNext()) {
        noteIter.next();
        if (noteIter.value() == nullptr) {
            noteIter.remove();
            continue;
        }

        auto pdfAnnotation = noteIter.value()->source();
        noteIter.value()->setX(pdfAnnotation->rect.x() * pageRate);
        noteIter.value()->setY((m_pageGeometry.height() - pdfAnnotation->rect.y()) * pageRate - pdfAnnotation->rect.height() * pageRate);
        noteIter.value()->setWidth(pdfAnnotation->rect.width() * pageRate);
        noteIter.value()->setHeight(pdfAnnotation->rect.height() * pageRate);
    }
}

QSizeF PdfPageContainer::requestedSize() const
{
    return m_requestedSize;
}

Qt::Orientation PdfPageContainer::orientation() const
{
    return m_orientation;
}

qreal PdfPageContainer::scale() const
{
    return m_scale;
}

bool PdfPageContainer::checked() const
{
    return m_checked;
}

void PdfPageContainer::setPageSource(QSharedPointer<BasePage> pageSource)
{
    if (!pageSource)
        return;

    if (m_pageSource == pageSource) {
        _correctSize();
        return;
    }

    m_pageSource = pageSource;

    m_pageSource->originalSize();
    auto sizeWatcher = new QFutureWatcher<QSizeF>(this);
    connect(sizeWatcher, &QFutureWatcher<QSizeF>::finished, this, [this, sizeWatcher]() {
        if (m_pageSource) {
            for(auto &tile : m_tilesMap.values())
                tile->setPageSource(m_pageSource);

            update();
        }

        sizeWatcher->deleteLater();
    });
    sizeWatcher->setFuture(m_pageSource->originalSize());

    _prepareBackgroundPage();

    _correctSize();

    if (!m_pageSource->isAnnotationsSupport())
        return;

    connect(m_pageSource.data(), &BasePage::annotationsLoaded, this, &PdfPageContainer::_loadAnnotations);
    connect(m_pageSource.data(), &BasePage::annotationAdded, this, [this](bool added) {
        if (m_pageSource && added) {
            m_pageSource->loadAnnotations();
            emit pageChanged();
        }
    });
    connect(m_pageSource.data(), &BasePage::annotationDelete, this, [this](int noteId, bool removeResult) {
        if (removeResult) {
            m_pageSource->loadAnnotations();
            emit pageChanged();
        }

        emit noteRemoved(noteId, removeResult);
    });
    connect(m_pageSource.data(), &BasePage::annotationEdited, this, [this](int noteId, bool edited) {
        Q_UNUSED(noteId)

        if (m_pageSource && edited) {
            m_pageSource->loadAnnotations();
            emit pageChanged();
        }

        emit noteEdited(noteId, edited);
    });

    if (m_pageSource->annotations().isEmpty())
        m_pageSource->loadAnnotations();
    else
        _loadAnnotations();
}

void PdfPageContainer::setPageGeometry(const PageGeometry &pg)
{
    m_pageGeometry = pg;
    _correctSize();
}

int PdfPageContainer::index() const
{
    return m_pageSource ? m_pageSource->pageNumber() : m_pageIndex;
}

void PdfPageContainer::setMapper(DocumentMapper *mapper)
{
    if (mapper == m_mapper || mapper == nullptr)
        return;

    m_mapper = mapper;
}

QSizeF PdfPageContainer::pageCurrentSize(const PageGeometry &pageGeometry,
                                         const QSizeF &requestedSize,
                                         Qt::Orientation orientation,
                                         qreal scale)
{
    auto heightToWidthRatio = pageGeometry.height() / pageGeometry.width();
    auto fitHeight = qMin(static_cast<qreal>(requestedSize.height()), requestedSize.width() * heightToWidthRatio);
    return {
        static_cast<qreal>((orientation == Qt::Vertical ? requestedSize.width() : fitHeight / heightToWidthRatio) * scale),
        static_cast<qreal>((orientation == Qt::Vertical ? requestedSize.width() * heightToWidthRatio : fitHeight) * scale)
    };
}

void PdfPageContainer::setVisibleArea(const QRectF &visibleArea)
{
    if (visibleArea == m_visibleArea)
        return;

    m_visibleArea = visibleArea;
}

bool PdfPageContainer::annotationsPaint() const
{
    return m_annotationsPaint;
}

bool PdfPageContainer::notesPaint() const
{
    return m_notesPaint;
}

bool PdfPageContainer::grayScaleRendering() const
{
    return m_grayScaleRendering;
}

QSharedPointer<BasePage> PdfPageContainer::source() const {
    return m_pageSource;
}

void PdfPageContainer::addAnnotation(const QRectF &rect, const QColor &color, const QString &author, const QString &content)
{
    if (!m_pageSource)
        return;

    auto pageRate = width() / m_pageGeometry.width();
    auto correctRect = QRectF();
    correctRect.setX(qMax(0.0f, float(rect.x() / pageRate)));
    correctRect.setY(qMax(0.0f, float((height() - rect.y() - rect.height()) / pageRate)));

    correctRect.setWidth(rect.width() / pageRate);
    if (correctRect.x() + correctRect.width() > m_pageGeometry.width())
        correctRect.setWidth(m_pageGeometry.width() - correctRect.x() - 1);

    correctRect.setHeight(rect.height() / pageRate);
    if (correctRect.y() + correctRect.height() > m_pageGeometry.height())
        correctRect.setHeight(m_pageGeometry.height() - correctRect.y() - 1);

    m_pageSource->addAnnotation(correctRect, color, author, content);
}

void PdfPageContainer::removeNote(int noteId)
{
    if (!m_pageSource)
        return;

    m_pageSource->removeAnnotation(noteId);
}

void PdfPageContainer::editNote(int noteId, const QString &newContent, const QColor &newColor)
{
    if (!m_pageSource)
        return;

    m_pageSource->editNote(noteId, newContent, newColor);
}

void PdfPageContainer::setRequestedSize(QSizeF requestedSize)
{
    if (m_requestedSize == requestedSize)
        return;

    m_requestedSize = requestedSize;
    emit requestedSizeChanged(m_requestedSize);
    _correctSize();
}

void PdfPageContainer::setOrientation(Qt::Orientation orientation)
{
    if (m_orientation == orientation)
        return;

    m_orientation = orientation;
    emit orientationChanged(m_orientation);

    _correctSize();
}

void PdfPageContainer::setScale(qreal scale)
{
    if (qFuzzyCompare(m_scale, scale))
        return;

    m_scale = scale;
    emit scaleChanged(m_scale);

    _correctSize();
}

void PdfPageContainer::setAnnotationsPaint(bool annotationsPaint)
{
    if (m_annotationsPaint == annotationsPaint)
        return;

    m_annotationsPaint = annotationsPaint;
    emit annotationsPaintChanged(m_annotationsPaint);

    if (m_annotationsItems.isEmpty())
        _loadAnnotations();
    else
        for (auto annotation : m_annotationsItems)
            annotation->setOpacity(m_annotationsPaint ? 1.0 : 0.0);
}

void PdfPageContainer::setNotesPaint(bool notesPaint)
{
    if (m_notesPaint == notesPaint)
        return;

    m_notesPaint = notesPaint;
    emit notesPaintChanged(m_notesPaint);

    if (m_notesItems.isEmpty())
        _loadAnnotations();
    else
        for (auto note : m_notesItems)
            note->setOpacity(m_notesPaint ? 1.0 : 0.0);
}

void PdfPageContainer::setGrayScaleRendering(bool grayScaleRendering)
{
    if (m_grayScaleRendering == grayScaleRendering) {
        return;
    }

    m_grayScaleRendering = grayScaleRendering;

    _tailorise();
}

void PdfPageContainer::setChecked(bool checked)
{
    if (m_checked == checked)
        return;

    m_checked = checked;
    emit checkedChanged(m_checked);
    emit sendCheckChangedInfo(m_pageIndex, m_checked);
}

void PdfPageContainer::setIndex(int pageIndex)
{
    if (m_pageIndex == pageIndex)
        return;

    m_pageIndex = pageIndex;
}

void PdfPageContainer::setCentering(bool centering)
{
    if (m_centering == centering)
        return;

    m_centering = centering;
}

void PdfPageContainer::setFitToPage(bool fitToPage)
{
    if (m_fitToPage == fitToPage)
        return;

    m_fitToPage = fitToPage;
}

void PdfPageContainer::_correctSize()
{
    auto heightToWidthRatio = m_pageGeometry.height() / m_pageGeometry.width();
    auto fitHeight = qMin(static_cast<qreal>(m_requestedSize.height()), m_requestedSize.width() * heightToWidthRatio);
    m_pageImageSize.setWidth((m_orientation == Qt::Vertical ? m_requestedSize.width() : fitHeight / heightToWidthRatio) * m_scale);
    m_pageImageSize.setHeight((m_orientation == Qt::Vertical ? m_requestedSize.width() * heightToWidthRatio : fitHeight) * m_scale);

    setWidth(m_pageImageSize.width());
    setHeight(m_pageImageSize.height());

    auto pageScale = qMin(m_pageImageSize.width() / m_pageGeometry.width(), m_pageImageSize.height() / m_pageGeometry.height());

    if (qFuzzyCompare(m_imageScale, pageScale) && !m_tilesMap.isEmpty())
        return;

    m_imageScale = pageScale;

    _tailorise();
}

void PdfPageContainer::_tailorise()
{
    auto heightToWidthRatio = m_pageImageSize.height() / m_pageImageSize.width();
    auto actualTileWidth = m_tileSize * m_scale;
    auto actualTileHeight = actualTileWidth * heightToWidthRatio;
    auto tileCountX = qCeil(m_pageImageSize.width() / actualTileWidth);
    auto tileCountY = qCeil(m_pageImageSize.height() / actualTileHeight);
    auto maxTileCount = qMax(tileCountX, tileCountY);

    if (m_tilesMap.size() != tileCountX * tileCountY) {
        for (auto &tile : m_tilesMap.values()) {
            tile->setEnabled(false);
            tile->setVisible(false);
            tile->deleteLater();
        }

        m_tilesMap.clear();
    }

    actualTileWidth = m_pageImageSize.width() / maxTileCount;
    actualTileHeight = m_pageImageSize.height() / maxTileCount;
    auto tileIndex = -1;
    auto oldMaxZ = m_maxTileZ;
    for (int tileX = 0; tileX < maxTileCount; ++tileX) {
        for (int tileY = 0; tileY < maxTileCount; ++tileY) {
            ++tileIndex;

            if (!m_tilesMap.contains(tileIndex)) {
                auto tile = new PdfPageTile(this);
                tile->setRenderable(false);
                m_tilesMap.insert(tileIndex, tile);
            }

            auto tile = m_tilesMap.value(tileIndex);
            tile->setRenderFlags(m_grayScaleRendering ? PdfPage::RenderFlags::GrayScale : PdfPage::RenderFlags::NoFlags);
            tile->setCentering(m_centering);
            tile->setFitToPage(m_fitToPage);
            tile->setImageScale(m_imageScale);
            tile->setActualPageSize(QSizeF(m_pageGeometry.width(), m_pageGeometry.height()));

            if (m_pageSource)
                tile->setPageSource(m_pageSource);

            QPointF tilePosition(tileX * actualTileWidth, tileY * actualTileHeight);
            tile->setX(tilePosition.x());
            tile->setY(tilePosition.y());

            auto currentWidth = tilePosition.x() + actualTileWidth;
            if (currentWidth <= m_pageImageSize.width())
                tile->setWidth(actualTileWidth);
            else
                tile->setWidth(actualTileWidth - qAbs(std::remainder(m_pageImageSize.width(), actualTileWidth)));

            auto currentHeight = tilePosition.y() + actualTileHeight;
            if (currentHeight <= m_pageImageSize.height())
                tile->setHeight(actualTileHeight);
            else
                tile->setHeight(actualTileHeight - qAbs(std::remainder(m_pageImageSize.height(), actualTileHeight)));

            m_maxTileZ = qMax(m_maxTileZ, tile->z());
        }
    }

    if (qFuzzyCompare(m_maxTileZ, oldMaxZ)) {
        for (auto annotationItem : m_annotationsItems)
            annotationItem->setZ(m_maxTileZ + 1);

        for (auto noteItem : m_notesItems)
            noteItem->setZ(m_maxTileZ + 1);
    }

    emit pageReady();

    m_allTilesReady = false;
}

void PdfPageContainer::_updateVisible()
{
    if (!isEnabled())
        return;

    if (!m_pageSource)
        return;

    if (m_mapper == nullptr)
        return;

    if (m_orientation == Qt::Vertical) {
        if (y() > m_visibleArea.height() || height() < -y()) {
            for (auto &tile : m_tilesMap.values())
                tile->setRenderable(false);

            return;
        }
    } else {
        if (x() > m_visibleArea.width() || width() < -x()) {
            for (auto &tile : m_tilesMap.values())
                tile->setRenderable(false);
            return;
        }
    }

    auto hVisible = 0.0f;
    auto wVisible = 0.0f;
    auto yVisible = 0.0f;
    auto xVisible = 0.0f;

    if (m_orientation == Qt::Vertical) {
        hVisible = y() > 0 ? (m_visibleArea.height() - y()) : (height() + y());
        wVisible = m_visibleArea.width();
        yVisible = y() > 0 ? 0.0f : -y();
        xVisible = x() > 0 ? 0.0f : -x();
    } else {
        hVisible = m_visibleArea.height();
        wVisible = x() > 0 ? (m_visibleArea.width() - x()) : (width() + x());
        yVisible = y() > 0 ? 0.0f : -y();
        xVisible = x() > 0 ? 0.0f : -x();
    }

    QRectF localVisibleArea(xVisible, yVisible, wVisible, hVisible);

    bool allTilesAreBitmap = true;
    for (auto &tile : m_tilesMap.values()) {
        QRectF tileRect(tile->x(), tile->y(), tile->width(), tile->height());
        auto tileVisible = localVisibleArea.intersects(tileRect);
        tile->setRenderable(tileVisible);

        if (!tile->isBitmap() && tileVisible)
            allTilesAreBitmap = false;
    }

    if (m_allTilesReady)
        return;

    if (allTilesAreBitmap) {
        for (auto &annotation : m_annotationsItems)
            annotation->setVisible(true);

        for (auto &note : m_notesItems)
            note->setVisible(true);

        m_allTilesReady = true;
    }
}

void PdfPageContainer::_loadAnnotations()
{
    if (!m_pageSource)
        return;

    for (auto &annotation : m_annotationsItems)
        annotation->deleteLater();
    m_annotationsItems.clear();

    for (auto &note : m_notesItems)
        note->deleteLater();
    m_notesItems.clear();

    auto pageRate = width() / m_pageGeometry.width();
    auto annotations = m_pageSource->annotations();
    for (const auto &annotation : annotations) {
        if (annotation == nullptr)
            continue;

        if (annotation->type == BaseAnnotation::AnnotationType::Link || annotation->type == BaseAnnotation::AnnotationType::Url) {
            auto annotationItem = new PdfSimpleAnnotation(this, annotation);
            connect(annotationItem, &PdfSimpleAnnotation::triggered, this, &PdfPageContainer::annotationActivate);
            connect(this, &PdfPageContainer::yChanged, annotationItem, &PdfSimpleAnnotation::clearHighlight);
            annotationItem->setOpacity(m_annotationsPaint ? 1.0 : 0.0);
            annotationItem->setX(annotation->rect.x() * pageRate);
            annotationItem->setY((m_pageGeometry.height() - annotation->rect.y()) * pageRate - annotation->rect.height() * pageRate);
            annotationItem->setWidth(annotation->rect.width() * pageRate);
            annotationItem->setHeight(annotation->rect.height() * pageRate);
            annotationItem->setZ(m_maxTileZ);
            annotationItem->setVisible(false);
            m_annotationsItems.append(annotationItem);

            continue;
        }

        if (annotation->type == BaseAnnotation::AnnotationType::HighLight
                || annotation->type == BaseAnnotation::AnnotationType::Text) {
            auto noteItem = new PdfSimpleNote(this, annotation);
            connect(noteItem, &PdfSimpleNote::triggered, this, &PdfPageContainer::_noteActivate);
            connect(this, &PdfPageContainer::yChanged, noteItem, &PdfSimpleNote::clearHighlight);
            noteItem->setOpacity(m_notesPaint ? 1.0 : 0.0);
            noteItem->setX(annotation->rect.x() * pageRate);
            noteItem->setY((m_pageGeometry.height() - annotation->rect.y()) * pageRate - annotation->rect.height() * pageRate);
            noteItem->setWidth(annotation->rect.width() * pageRate);
            noteItem->setHeight(annotation->rect.height() * pageRate);
            noteItem->setZ(m_maxTileZ);
            noteItem->setVisible(false);
            m_notesItems.append(noteItem);

            continue;
        }
    }
}

void PdfPageContainer::_prepareBackgroundPage()
{
    if (!m_pageSource)
        return;

    if (m_backgroundPage) {
        m_backgroundPage->setRenderable(false);
        m_backgroundPage->deleteLater();
    }

    m_backgroundPage = new PdfBackgroundPage(this);

    m_backgroundPage->setPageSource(m_pageSource);
    m_backgroundPage->setActualPageSize(QSizeF(m_pageGeometry.width(), m_pageGeometry.height()));

    m_backgroundPage->setCentering(m_centering);
    m_backgroundPage->setFitToPage(m_fitToPage);
    m_backgroundPage->setImageScale(m_imageScale);
    m_backgroundPage->setPageScale(m_scale);
    m_backgroundPage->setWidth(width());
    m_backgroundPage->setHeight(height());
    m_backgroundPage->setRenderable(true);
    m_backgroundPage->setZ(-1);
}

void PdfPageContainer::_noteActivate(QString noteText, QString author)
{
    emit noteActivate(noteText, author, m_pageSource->pageNumber());
}
