// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QQmlExtensionPlugin>
#include <QtQml>

#include "basedocument.h"
#include "bookmarksmodel.h"
#include "pageswithnotesmodel.h"
#include "pagewithnotessortmodel.h"
#include "pdfdocumentitem.h"
#include "pdfpagecontainer.h"
#include "pdfview.h"

class Q_DECL_EXPORT AmberpdfQmlPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ru.omp.amberpdf")
public:
    void initializeEngine(QQmlEngine *engine, const char *uri) override
    {
        Q_UNUSED(uri)
        Q_UNUSED(engine)
    }

    void registerTypes(const char *uri) override
    {
        // @uri ru.omp.amberpdf
        qmlRegisterType<PdfView>(uri, 1, 0, "PdfViewPrivate");
        qmlRegisterType<PdfDocumentItem>(uri, 1, 0, "PdfDocument");
        qmlRegisterType<PdfPageContainer>(uri, 1, 0, "PdfPageContainer");
        qmlRegisterType<BookmarksModel>(uri, 1, 0, "BookmarksModel");
        qmlRegisterType<PagesWithNotesModel>(uri, 1, 0, "PagesWithNotesModel");
        qmlRegisterType<PageWithNotesSortModel>(uri, 1, 0, "PageWithNotesSortModel");
        qmlRegisterInterface<BasePdfDocument>("BasePdfDocument");
    }
};

#include "plugin.moc"
