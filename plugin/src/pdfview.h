// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PDFVIEW_H
#define PDFVIEW_H

#include <QQuickItem>

#include "basedocument.h"

class DocumentMapper;
class BaseAnnotation;
class BookmarksModel;
class PagesWithNotesModel;
class QQmlComponent;
class PdfPageContainer;

class PdfView : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(qreal contentHeight READ contentHeight NOTIFY contentHeightChanged)
    Q_PROPERTY(qreal contentWidth READ contentWidth NOTIFY contentWidthChanged)
    Q_PROPERTY(qreal contentY READ contentY WRITE setContentY NOTIFY contentYChanged)
    Q_PROPERTY(qreal contentX READ contentX WRITE setContentX NOTIFY contentXChanged)
    Q_PROPERTY(int currentIndex READ currentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(qreal catchBound READ catchBound WRITE setCatchBound NOTIFY catchBoundChanged)
    Q_PROPERTY(Qt::Orientation orientation READ orientation WRITE setOrientation NOTIFY orientationChanged)
    Q_PROPERTY(qreal lastPageIndent READ lastPageIndent NOTIFY lastPageIndentChanged)
    Q_PROPERTY(qreal itemScale READ itemScale WRITE setItemScale NOTIFY itemScaleChanged)
    Q_PROPERTY(bool annotationsPaint READ annotationsPaint WRITE setAnnotationsPaint NOTIFY annotationsPaintChanged)
    Q_PROPERTY(bool notesPaint READ notesPaint WRITE setNotesPaint NOTIFY notesPaintChanged)
    Q_PROPERTY(BasePdfDocument *documentProvider READ documentProvider WRITE setDocumentProvider
                   NOTIFY documentProviderChanged)
    Q_PROPERTY(QString pdfPath READ pdfPath WRITE setPdfPath NOTIFY pdfPathChanged)
    Q_PROPERTY(QString pageRange READ pageRange WRITE setPageRange NOTIFY pageRangeChanged)
    Q_PROPERTY(BasePdfDocument::DocumentStatus status READ status NOTIFY statusChanged)
    Q_PROPERTY(qreal contentTopMargin READ contentTopMargin NOTIFY contentTopMarginChanged)
    Q_PROPERTY(BookmarksModel* bookmarksModel READ bookmarksModel NOTIFY bookmarksModelChanged)
    Q_PROPERTY(int fileVersion READ fileVersion NOTIFY fileVersionChanged)
    Q_PROPERTY(bool documentEdited READ documentEdited NOTIFY documentEditedChanged)
    Q_PROPERTY(PagesWithNotesModel* pagesWithNotesModel READ pagesWithNotesModel NOTIFY pagesWithNotesModelChanged)
    Q_PROPERTY(QQmlComponent *pageNumberComponent READ pageNumberComponent WRITE setPageNumberComponent NOTIFY pageNumberComponentChanged)
    Q_PROPERTY(bool grayScaleRendering READ grayScaleRendering WRITE setGrayScaleRendering NOTIFY grayScaleRenderingChanged)
    Q_PROPERTY(qreal pageAspectRatio READ pageAspectRatio WRITE setPageAspectRatio NOTIFY pageAspectRatioChanged)
    Q_PROPERTY(bool reverse READ reverse WRITE setReverse NOTIFY reverseChanged)
    Q_PROPERTY(QQmlComponent *pageCheckComponent READ pageCheckComponent WRITE setPageCheckComponent
                   NOTIFY pageCheckComponentChanged)
    Q_PROPERTY(qreal hiddenPagesOpacity READ hiddenPagesOpacity WRITE setHiddenPagesOpacity NOTIFY
                   hiddenPagesOpacityChanged)
    Q_PROPERTY(int specialPagesCount READ specialPagesCount NOTIFY specialPagesCountChanged)
    Q_PROPERTY(qreal pageSpacing READ pageSpacing WRITE setPageSpacing NOTIFY pageSpacingChanged)
    Q_PROPERTY(int maxPageIndex READ maxPageIndex NOTIFY maxPageIndexChanged)
    Q_PROPERTY(int minPageIndex READ minPageIndex NOTIFY minPageIndexChanged)
    Q_PROPERTY(bool fitToPage READ fitToPage WRITE setFitToPage NOTIFY fitToPageChanged)
    Q_PROPERTY(bool centering READ centering WRITE setCentering NOTIFY centeringChanged)

public:
    explicit PdfView(QQuickItem *parent = nullptr);
    ~PdfView() override;

    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;

    int count() const;
    qreal contentHeight() const;
    qreal contentWidth() const;
    qreal contentY() const;
    qreal contentX() const;
    int currentIndex() const;
    qreal catchBound() const;
    Qt::Orientation orientation() const;
    qreal lastPageIndent() const;
    qreal itemScale() const;
    bool annotationsPaint() const;
    bool notesPaint() const;
    BasePdfDocument *documentProvider() const;
    QString pdfPath() const;
    QString pageRange() const;
    bool grayScaleRendering() const;
    qreal pageAspectRatio() const;
    BasePdfDocument::DocumentStatus status() const;
    qreal contentTopMargin() const;
    BookmarksModel *bookmarksModel() const;
    int fileVersion() const;

    Q_INVOKABLE void saveDocumentAs(const QString &path);
    bool documentEdited() const;
    Q_INVOKABLE void addAnnotation(const QRectF &rect,
                                   const QColor &color,
                                   const QString &author,
                                   const QString &content);
    Q_INVOKABLE void removeNote(int pageId, int noteId);
    Q_INVOKABLE void editNote(int pageId, int noteId, const QString &newContent, const QColor &newColor);
    PagesWithNotesModel* pagesWithNotesModel() const;
    QQmlComponent *pageNumberComponent() const;
    bool reverse() const;
    QQmlComponent *pageCheckComponent() const;
    qreal hiddenPagesOpacity() const;
    int specialPagesCount() const;
    qreal pageSpacing() const;
    int maxPageIndex() const;
    int minPageIndex() const;
    bool fitToPage() const;
    bool centering() const;

public slots:
    void setContentY(qreal contentY);
    void setContentX(qreal contentX);
    void setCatchBound(qreal catchBound);
    void positionViewAtIndex(int index);
    void setOrientation(Qt::Orientation orientation);
    void setItemScale(qreal itemScale);
    void scaleAroundPoint(const QPointF &center, qreal newScale);
    void clicked(const QPointF &point);
    void setAnnotationsPaint(bool annotationsPaint);
    void setNotesPaint(bool notesPaint);
    void setDocumentProvider(BasePdfDocument *documentProvider);
    void setPdfPath(QString pdfPath);
    void setPageRange(const QString &pageRange);
    void setPageNumberComponent(QQmlComponent *pageNumberComponent);
    void setGrayScaleRendering(bool grayScaleRendering);
    void setPageAspectRatio(qreal pageAspectRatio);
    void setReverse(bool reverse);
    void setPageCheckComponent(QQmlComponent *pageCheckComponent);
    void setHiddenPagesOpacity(qreal hiddenPagesOpacity);
    void setPageSpacing(qreal pageSpacing);
    void setFitToPage(bool fitToPage);
    void setCentering(bool centering);

signals:
    void modelChanged(QObject* model);
    void countChanged(int count);
    void contentHeightChanged(qreal contentHeight);
    void contentWidthChanged(qreal contentWidth);
    void contentYChanged(qreal contentY);
    void contentXChanged(qreal contentX);
    void loadPages();
    void currentIndexChanged(int currentIndex);
    void catchBoundChanged(qreal catchBound);
    void contentChanged();
    void orientationChanged(Qt::Orientation orientation);
    void lastPageIndentChanged(qreal lastPageIndent);
    void itemScaleChanged(qreal itemScale);
    void annotationsPaintChanged(bool annotationsPaint);
    void clickedUrl(QString url) const;
    void clickedGoToPage(int pageNumber, QPointF coordinateOnPage) const;
    void notesPaintChanged(bool notesPaint);
    void noteActivated(QString noteText, QString author, int pageIndex);
    void documentProviderChanged(BasePdfDocument *documentProvider);
    void pdfPathChanged(QString pdfPath);
    void pageRangeChanged(QString pageRange);
    void statusChanged(BasePdfDocument::DocumentStatus status);
    void contentTopMarginChanged(qreal contentTopMargin);
    void bookmarksModelChanged(BookmarksModel *bookmarksModel);
    void fileVersionChanged(int fileVersion);
    void documentSaved(bool saveStatus);
    void documentEditedChanged(bool documentEdited);
    void noteRemoved(int pageIndex, int noteId, bool result);
    void noteEdited(int pageIndex, int noteId, bool result);
    void pagesWithNotesModelChanged(PagesWithNotesModel* pagesWithNotesModel);
    void pageNumberComponentChanged(QQmlComponent *pageNumberComponent);
    void grayScaleRenderingChanged(bool grayScaleRendering);
    void pageAspectRatioChanged(qreal pageAspectRatio);
    void reverseChanged(bool reverse);
    void pageCheckComponentChanged(QQmlComponent *pageCheckComponent);
    void hiddenPagesOpacityChanged(qreal hiddenPagesOpacity);
    void specialPagesCountChanged(int specialPagesCount);
    void pageSpacingChanged(qreal pageSpacing);
    void maxPageIndexChanged(int maxPageIndex);
    void minPageIndexChanged(int minPageIndex);
    void fitToPageChanged(bool fitToPage);
    void centeringChanged(bool centering);

private slots:
    void _updateContentSize();
    void _positionPages();
    void _updateAbsolutePosition();
    void _restoreAbsolutePosition();
    void _preparePages();
    void _preparePageChecker(PdfPageContainer *page, int pageIndex);
    void _calculateVisible();
    void _updateCurrentIndex();
    void _processActivatedAnnotation(BaseAnnotation *annotation);
    void _loadDocument();
    void _documentEdited();
    void _pageChecked(int index, bool checked);
    void _updateSpecailIndexes();
    void _updateSpecialPagesCount(int specialPagesCount);

signals:
    void _specialIndexesChanged();

private:
    DocumentMapper *m_mapper;
    int m_count;
    qreal m_contentHeight;
    qreal m_contentWidth;
    qreal m_contentY;
    qreal m_contentX;
    qreal m_moveDirection;
    QHash<int, QQuickItem *> m_pages;
    QList<int> m_paintedPages;
    QPointF m_absolutePosition;
    qreal m_paintedItemsSize;
    int m_currentIndex;
    qreal m_catchBound;
    Qt::Orientation m_orientation;
    qreal m_lastPageIndent;
    qreal m_itemScale;
    bool m_annotationsPaint;
    bool m_notesPaint;
    BasePdfDocument *m_documentProvider;
    QString m_pdfPath;
    QString m_pageRange;
    qreal m_contentTopMargin;
    QTimer *m_timer;
    BookmarksModel *m_bookmarksModel;
    bool m_documentEdited;
    PagesWithNotesModel *m_pagesWithNotesModel;
    QHash<int, QQuickItem *> m_pageNumberItems;
    QQmlComponent *m_pageNumberComponent;
    bool m_grayScaleRendering;
    qreal m_pageAspectRatio;
    bool m_reverse;
    QQmlComponent *m_pageCheckComponent;
    QHash<int, QQuickItem *> m_pageCheckerItems;
    qreal m_hiddenPagesOpacity;
    int m_specialPagesCount;
    qreal m_pageSpacing;
    bool m_fitToPage;
    bool m_centering;
};

#endif // PDFVIEW_H
