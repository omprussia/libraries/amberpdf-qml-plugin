// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef BASEPAGE_H
#define BASEPAGE_H

#include <QObject>
#include <QSizeF>
#include <QFuture>
#include <QPointF>

class BaseAnnotation;
class BasePage : public QObject
{
    Q_OBJECT

public:
    explicit BasePage(QObject *parent = nullptr);
    virtual ~BasePage();

    virtual QList<BaseAnnotation *> annotations() const = 0;
    virtual void loadAnnotations() = 0;
    virtual bool isAnnotationsSupport() const = 0;
    virtual int pageNumber() const = 0;
    virtual QFuture<QSizeF> originalSize() = 0;
    virtual QFuture<QImage> bitmapFull(qreal pageScale, int renderFlags = 0) const = 0;
    virtual QFuture<QImage> bitmapPart(qreal pageScaleX, qreal pageScaleY,
                                       int renderFlags = 0, qreal zoom = 1.0,
                                       const QPointF &bias = QPointF()) const = 0;
    virtual void addAnnotation(const QRectF &rect, const QColor &color,
                                       const QString &author, const QString &content) = 0;
    virtual void removeAnnotation(int annotationId) = 0;
    virtual void editNote(int noteId, const QString &newContent, const QColor &newColor) = 0;

signals:
    void annotationsLoaded();
    void originalSizeLoaded();
    void annotationAdded(bool);
    void annotationDelete(int, bool);
    void annotationEdited(int, bool);
};

#endif // BASEPAGE_H
