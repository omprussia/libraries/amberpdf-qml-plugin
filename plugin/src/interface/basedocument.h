// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef BASEDOCUMENT_H
#define BASEDOCUMENT_H

#include <QObject>
#include <QSharedPointer>

class BasePage;
class BaseBookmark;
class BasePdfDocument : public QObject
{
    Q_OBJECT

    Q_PROPERTY(DocumentStatus status READ status NOTIFY statusChanged)
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)

public:
    enum class DocumentStatus
    {
        Null,
        Loading,
        Ready,
        Error
    };
    Q_ENUM(DocumentStatus)

    enum class PageLoadStatus
    {
        Success,
        Fail
    };
    Q_ENUM(PageLoadStatus)

    explicit BasePdfDocument(QObject *parent = nullptr);
    virtual ~BasePdfDocument();

    DocumentStatus status() const;

    virtual QString path() const;
    virtual QSizeF pageSize(int pageNumber) const = 0;
    virtual int count() const = 0;
    virtual void loadAllPages() = 0;
    virtual QSharedPointer<BasePage> loadPage(int pageIndex) = 0;
    virtual void startLoadBookmarks() const = 0;
    virtual QVector<BaseBookmark *> bookmarks() const = 0;
    virtual int fileVersion() const = 0;
    virtual bool saveDocumentAs(const QString &path) const = 0;

public slots:
    virtual void setPath(const QString &path) = 0;

signals:
    void statusChanged(DocumentStatus status);
    void pathChanged(const QString &path);
    void pageLoaded(int pageNumber, PageLoadStatus loadStatus);
    void bookmarksLoaded();
    void fileVersionChanged(int version);

protected:
    DocumentStatus m_status;
    QString m_path;
};

#endif // BASEDOCUMENT_H
