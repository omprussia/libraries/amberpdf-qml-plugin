// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "basepage.h"

BasePage::BasePage(QObject *parent) : QObject(parent) {  }

BasePage::~BasePage() = default;
