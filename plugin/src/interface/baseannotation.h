// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef BASEANNOTATION_H
#define BASEANNOTATION_H

#include <QRectF>
#include <QString>
#include <QColor>

struct BaseAnnotation
{
public:
    enum class AnnotationType
    {
        Link,
        Url,
        HighLight,
        Text
    };

    AnnotationType type;
    QRectF rect;
    QString author;
    QString content;
    int linkToPage;
    QPointF pageCoordinate;
    QColor color{Qt::white};
    int annotationId;
};

#endif // BASEANNOTATION_H
