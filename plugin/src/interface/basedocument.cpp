// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QMetaType>

#include "basedocument.h"

BasePdfDocument::BasePdfDocument(QObject *parent)
    : QObject(parent)
{
    qRegisterMetaType<PageLoadStatus>();
}

BasePdfDocument::~BasePdfDocument() = default;

BasePdfDocument::DocumentStatus BasePdfDocument::status() const
{
    return m_status;
}

QString BasePdfDocument::path() const
{
    return m_path;
}
