// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef BASEBOOKMARK_H
#define BASEBOOKMARK_H

#include <QString>
#include <QPointF>

struct BaseBookmark
{
    QString title;
    int pageIndex {-1};
    int level {-1};
    QPointF positionInPage;
};

#endif // BASEBOOKMARK_H
