// SPDX-FileCopyrightText: 2022-2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PDFSIMPLENOTE_H
#define PDFSIMPLENOTE_H

#include <QQuickItem>
#include <QImage>

class QSGTexture;
class BaseAnnotation;
class PdfSimpleNote : public QQuickItem
{
    Q_OBJECT
public:
    PdfSimpleNote(QQuickItem *parent = nullptr, BaseAnnotation *source = nullptr);

    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;

    BaseAnnotation *source() const;

public slots:
    bool event(QEvent *event) override;
    void clearHighlight();

signals:
    void triggered(QString, QString);

private:
    void _setHighlight(bool highlight);

private:
    bool m_highlighted;
    bool m_needUpdateImage;
    quint16 m_padding;
    QImage m_paper;
    BaseAnnotation *m_noteSource;
};

#endif // PDFSIMPLENOTE_H
