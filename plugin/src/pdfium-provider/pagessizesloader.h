// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PAGESSIZESLOADER_H
#define PAGESSIZESLOADER_H

#include <QObject>
#include <QHash>
#include <QSet>
#include <QRunnable>
#include <QSizeF>
#include <QSharedPointer>

class PdfDocument;
class PagesSizesLoader : public QObject, public QRunnable
{
    Q_OBJECT

public:
    PagesSizesLoader(QSharedPointer<PdfDocument> pdfiumDocument);

    void run() override;

signals:
    void done(QHash<int, QSizeF>);

private:
    QSharedPointer<PdfDocument> m_pdfiumDocument;
    QHash<int, QSizeF> m_pageSizes;
};

#endif // PAGESSIZESLOADER_H
