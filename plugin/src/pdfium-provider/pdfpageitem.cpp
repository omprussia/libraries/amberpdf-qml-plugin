/****************************************************************************
**
** Copyright (C) 2022-2024 Open Mobile Platform LLC.
** Contact: https://community.omprussia.ru/open-source
**
** This file is part of the AmberPDF-QML-Plugin project.
**
** $QT_BEGIN_LICENSE:BSD$
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform LLC copyright holder nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QFutureWatcher>

#include <amberpdf/pdfpage.h>
#include <amberpdf/pdfannotation.h>

#include <baseannotation.h>

#include "pdfpageitem.h"

PdfPageItem::PdfPageItem(QSharedPointer<PdfPage> amberPage, QObject *parent) : BasePage(parent),
    m_amberPage(amberPage)
{  }

PdfPageItem::~PdfPageItem()
{
    qDeleteAll(m_annotations);
}

QList<BaseAnnotation *> PdfPageItem::annotations() const
{
    return m_annotations;
}

void PdfPageItem::loadAnnotations()
{
    auto annotations = m_amberPage->annotations();
    auto *watcher = new QFutureWatcher<QList<QObject *>>(this->parent());
    watcher->setFuture(annotations);
    connect(watcher, &QFutureWatcher<QList<QObject *>>::finished, this, [this, watcher]() {
        if (watcher == nullptr)
            return;

        if (watcher->isFinished() && !watcher->isCanceled()) {
            if (!m_annotations.isEmpty()) {
                qDeleteAll(m_annotations);
                m_annotations.clear();
            }

            auto annotationIndex = -1;
            for (const auto &annotation : watcher->result()) {
                ++annotationIndex;

                auto pdfAnnotation = qobject_cast<PdfAnnotation *>(annotation);
                if (pdfAnnotation == nullptr)
                    continue;

                BaseAnnotation *baseAnnotation = nullptr;

                if (pdfAnnotation->type() == PdfAnnotation::Link) {
                    baseAnnotation = new BaseAnnotation();
                    baseAnnotation->type = pdfAnnotation->uri().isEmpty()
                            ? BaseAnnotation::AnnotationType::Link
                            : BaseAnnotation::AnnotationType::Url;

                    baseAnnotation->linkToPage = pdfAnnotation->linkToPage();
                    baseAnnotation->pageCoordinate = pdfAnnotation->linkPosition();
                    baseAnnotation->content = pdfAnnotation->uri();
                }

                if (pdfAnnotation->type() == PdfAnnotation::Highlight || pdfAnnotation->type() == PdfAnnotation::Text) {
                    baseAnnotation = new BaseAnnotation();
                    baseAnnotation->type = BaseAnnotation::AnnotationType::HighLight;
                    baseAnnotation->author = pdfAnnotation->values().value(
                                pdfAnnotation->annotationKeyToQString(PdfAnnotation::T)).toString();
                    baseAnnotation->content = pdfAnnotation->values().value(
                                pdfAnnotation->annotationKeyToQString(PdfAnnotation::Contents)).toString();

                    static auto correctColor = [](const QColor &color) -> bool {
                        return (color != Qt::white && color != Qt::black);
                    };

                    auto annotationColor = pdfAnnotation->color();
                    annotationColor.setAlpha(255);
                    if (correctColor(annotationColor)) {
                        baseAnnotation->color = annotationColor;
                    } else {
                        annotationColor = pdfAnnotation->interiorColor();
                        annotationColor.setAlpha(255);

                        if (correctColor(annotationColor)) {
                            baseAnnotation->color = annotationColor;
                        } else {
                            foreach (auto colorPair, pdfAnnotation->objectsColors()) {
                                colorPair.first.setAlpha(255);
                                if (correctColor(colorPair.first)) {
                                    annotationColor = colorPair.first;
                                    break;
                                }

                                colorPair.second.setAlpha(255);
                                if (correctColor(colorPair.second)) {
                                    annotationColor = colorPair.second;
                                    break;
                                }
                            }
                        }
                    }

                    baseAnnotation->color = annotationColor;
                    baseAnnotation->color.setAlpha(255);
                }

                if (baseAnnotation != nullptr) {
                    baseAnnotation->rect = pdfAnnotation->rect();
                    baseAnnotation->annotationId = annotationIndex;
                    m_annotations.append(baseAnnotation);
                }
            }

            if (!m_annotations.isEmpty())
                emit annotationsLoaded();
        }

        watcher->deleteLater();
    });
}

bool PdfPageItem::isAnnotationsSupport() const
{
    return true;
}

int PdfPageItem::pageNumber() const
{
    return m_amberPage ? m_amberPage->pageNumber() : -1;
}

QFuture<QSizeF> PdfPageItem::originalSize()
{
    return m_amberPage ? m_amberPage->originalSize() : QFuture<QSizeF>();
}

QFuture<QImage> PdfPageItem::bitmapFull(qreal pageScale, int renderFlags) const
{
    return m_amberPage ? m_amberPage->bitmapFull(pageScale, renderFlags) : QFuture<QImage>();
}

QFuture<QImage> PdfPageItem::bitmapPart(qreal pageScaleX, qreal pageScaleY, int renderFlags, qreal zoom, const QPointF &bias) const
{
    return m_amberPage ? m_amberPage->bitmapPart(pageScaleX, pageScaleY, renderFlags, zoom, bias) : QFuture<QImage>();
}

void PdfPageItem::addAnnotation(const QRectF &rect, const QColor &color, const QString &author, const QString &content)
{
    if (!m_amberPage)
        return;

    auto *watcher = new QFutureWatcher<bool>();
    connect(watcher, &QFutureWatcher<bool>::finished, this, [this, watcher]() {
        if (watcher == nullptr)
            return;

        if (watcher->isFinished() && !watcher->isCanceled())
            emit annotationAdded(true);
        else
            emit annotationAdded(false);

        watcher->deleteLater();
    });

    auto future = m_amberPage->addAnnotation(rect, color, author, content);
    watcher->setFuture(future);
}

void PdfPageItem::removeAnnotation(int annotationId)
{
    if (!m_amberPage)
        return;

    auto *watcher = new QFutureWatcher<bool>();
    connect(watcher, &QFutureWatcher<bool>::finished, this, [this, watcher, annotationId]() {
        if (watcher == nullptr)
            return;

        if (watcher->isFinished() && !watcher->isCanceled())
            emit annotationDelete(annotationId, watcher->result());
        else
            emit annotationDelete(annotationId, false);
    });

    watcher->setFuture(m_amberPage->removeAnnotation(annotationId));
}

void PdfPageItem::editNote(int noteId, const QString &newContent, const QColor &newColor)
{
    BaseAnnotation *annotationToEdit = nullptr;
    for (const auto annotation : m_annotations) {
        if (annotation == nullptr)
            continue;

        if (annotation->annotationId == noteId) {
            annotationToEdit = annotation;
            break;
        }
    }

    if (annotationToEdit == nullptr)
        return;

    auto *watcher = new QFutureWatcher<bool>();
    connect(watcher, &QFutureWatcher<bool>::finished, this, [this, watcher, noteId]() {
        if (watcher == nullptr)
            return;

        if (watcher->isFinished() && !watcher->isCanceled())
            removeAnnotation(noteId);

        watcher->deleteLater();
    });

    auto future = m_amberPage->addAnnotation(annotationToEdit->rect, newColor, annotationToEdit->author, newContent);
    watcher->setFuture(future);

    connect(this, &PdfPageItem::annotationDelete, this, [this, noteId](int annotationId, bool result) {
        if (noteId != annotationId)
            return;

        emit annotationEdited(noteId, result);
    });
}
