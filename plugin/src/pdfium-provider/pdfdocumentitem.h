// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PDFDOCUMENTITEM_H
#define PDFDOCUMENTITEM_H

#include <QObject>
#include <QSharedPointer>
#include <QHash>
#include <QSet>
#include <QVector>
#include <QPointer>

#include "basedocument.h"
#include "pdfpageitem.h"
#include "pagepreloader.h"

class PdfPage;
class PdfDocument;
class PdfDocumentItem : public BasePdfDocument
{
    Q_OBJECT

public:
    explicit PdfDocumentItem(QObject *parent = nullptr);
    ~PdfDocumentItem();

    QString path() const override;
    QSizeF pageSize(int pageNumber) const override;
    Q_INVOKABLE int count() const override;
    void loadAllPages() override;
    QSharedPointer<BasePage> loadPage(int pageIndex) override;
    void startLoadBookmarks() const override;
    QVector<BaseBookmark *> bookmarks() const override;
    int fileVersion() const override;
    bool saveDocumentAs(const QString &path) const override;

public slots:
    void setPath(const QString &path) override;

private:
    void onPagePreloaderDone(int loadedPageIndex, PageLoadStatus loadStatus);

private:
    QSharedPointer<PdfDocument> m_pdfiumDocument;
    QHash<int, QSizeF> m_pageSizes;
    QHash<int, QSharedPointer<BasePage>> m_loadedPages;
    QSet<int> m_pagesInProcess;
    QVector<BaseBookmark *> m_baseBookmarks;
    QPointer<PagePreloader> m_preloaderAllPage;
};

#endif // PDFDOCUMENTITEM_H
