// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PdfPageItem_H
#define PdfPageItem_H

#include <QObject>
#include <QSharedPointer>

#include <basepage.h>

class PdfPage;
class BaseAnnotation;
class PdfPageItem : public BasePage
{
    Q_OBJECT
public:
    explicit PdfPageItem(QSharedPointer<PdfPage> amberPage, QObject *parent = nullptr);
    ~PdfPageItem();

    QList<BaseAnnotation *> annotations() const override;
    void loadAnnotations() override;
    bool isAnnotationsSupport() const override;
    int pageNumber() const override;
    QFuture<QSizeF> originalSize() override;
    QFuture<QImage> bitmapFull(qreal pageScale, int renderFlags = 0) const override;
    QFuture<QImage> bitmapPart(qreal pageScaleX, qreal pageScaleY,
                                       int renderFlags = 0, qreal zoom = 1.0,
                                       const QPointF &bias = QPointF()) const override;
    void addAnnotation(const QRectF &rect, const QColor &color,
                                   const QString &author, const QString &content) override;
    void removeAnnotation(int annotationId) override;
    void editNote(int noteId, const QString &newContent, const QColor &newColor) override;

private:
    QSharedPointer<PdfPage> m_amberPage;
    QList<BaseAnnotation *> m_annotations;
};

#endif // PdfPageItem_H
