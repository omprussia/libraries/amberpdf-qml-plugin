// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QMutexLocker>

#include <amberpdf/pdfdocument.h>

#include "pagepreloader.h"

QMutex PagePreloader::m_mutex;

PagePreloader::PagePreloader(QSharedPointer<PdfDocument> document, int pageIndex, int countPages) :
    m_document(document),
    m_pageIndex(pageIndex),
    m_count(countPages),
    m_isCanceled(false)
{  }

PagePreloader::~PagePreloader() = default;

void PagePreloader::run()
{
    if (!m_document || m_document->status() != PdfDocument::Success) {
        emit done(m_pageIndex, BasePdfDocument::PageLoadStatus::Fail);
        return;
    }

    for (int i = 0; i < m_count; i++) {

        if (m_isCanceled)
            break;

        QMutexLocker lock(&m_mutex);
        auto pageFuture = m_document->page(m_pageIndex + i);
        pageFuture.waitForFinished();
        if (pageFuture.isFinished() && !pageFuture.isCanceled())
            emit done(m_pageIndex + i, BasePdfDocument::PageLoadStatus::Success);
        else
            emit done(m_pageIndex + i, BasePdfDocument::PageLoadStatus::Fail);
    }
}

void PagePreloader::cancel()
{
    m_isCanceled = true;
}
