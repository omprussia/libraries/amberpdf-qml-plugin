// SPDX-FileCopyrightText: 2022-2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <amberpdf/pdfdocument.h>

#include "pagessizesloader.h"

PagesSizesLoader::PagesSizesLoader(QSharedPointer<PdfDocument> pdfiumDocument) :
    m_pdfiumDocument(pdfiumDocument)
{  }

void PagesSizesLoader::run()
{
    auto pagesCount = m_pdfiumDocument->pageCount();
    m_pageSizes.clear();

    for (int i = 0; i < pagesCount; ++i) {
        auto sizeFuture = m_pdfiumDocument->pageSize(i);
        sizeFuture.waitForFinished();
        m_pageSizes.insert(i, sizeFuture.result());

        // TODO: preload pages
        if (i < PRELOAD_PAGES_COUNT) {
            auto pageFuture = m_pdfiumDocument->page(i);
            pageFuture.waitForFinished();
        }
    }

    emit done(m_pageSizes);
}

