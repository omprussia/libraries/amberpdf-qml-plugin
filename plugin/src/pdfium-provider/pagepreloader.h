// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PAGEPRELOADER_H
#define PAGEPRELOADER_H

#include <QObject>
#include <QRunnable>
#include <QSharedPointer>
#include <QMutex>

#include "basedocument.h"

class PdfDocument;
class PagePreloader : public QObject, public QRunnable
{
    Q_OBJECT

public:
    PagePreloader(QSharedPointer<PdfDocument> document, int pageIndex, int countPages = 1);
    ~PagePreloader() override;

    void run() override;
    void cancel();

signals:
    void done(int, BasePdfDocument::PageLoadStatus);

private:
    QSharedPointer<PdfDocument> m_document;
    int m_pageIndex;
    int m_count;
    bool m_isCanceled;
    static QMutex m_mutex;
};

#endif // PAGEPRELOADER_H
