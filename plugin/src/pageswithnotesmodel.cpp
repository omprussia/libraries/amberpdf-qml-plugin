// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "interface/basedocument.h"

#include "notesmodel.h"
#include "pageswithnotesmodel.h"

PagesWithNotesModel::PagesWithNotesModel(QObject *parent) : QAbstractListModel(parent) { }

int PagesWithNotesModel::rowCount(const QModelIndex &parent) const
{
    if (m_documentProvider == nullptr)
        return 0;

    return parent.isValid() ? 0 : m_documentProvider->count();
}

QVariant PagesWithNotesModel::data(const QModelIndex &index, int role) const
{
    if (m_documentProvider == nullptr)
        return {  };

    if (!index.isValid() || index.row() > m_documentProvider->count())
        return {  };

    NotesModel *notesModel = nullptr;
    if (m_notesModels.contains(index.row()) && m_notesModels.value(index.row()) != nullptr)
        notesModel = m_notesModels.value(index.row());

    if (notesModel == nullptr)
        m_documentProvider->loadPage(index.row());

    auto modelLoading = notesModel == nullptr || notesModel->isLoading();

    switch (role) {
    case LoadingRole:
        return QVariant::fromValue(modelLoading ? true : false);
    case NotesModelRole:
        return (modelLoading ? QVariant() : QVariant::fromValue(notesModel));
    case CountRole:
        return QVariant::fromValue(modelLoading ? 0 : notesModel->rowCount(QModelIndex()));
    case PageIndexRole:
        return QVariant::fromValue(index.row());
    }

    return {  };
}

QHash<int, QByteArray> PagesWithNotesModel::roleNames() const
{
    return {
        { LoadingRole, "loading" },
        { NotesModelRole, "notesModel" },
        { CountRole, "notesCount" },
        { PageIndexRole, "pageIndex" }
    };
}

void PagesWithNotesModel::setNewData(BasePdfDocument *provider)
{
    if (provider == nullptr || provider == m_documentProvider)
        return;

    m_documentProvider = provider;
    connect(m_documentProvider, &BasePdfDocument::pageLoaded, this, &PagesWithNotesModel::_addPage);
    beginResetModel();
    endResetModel();
}

void PagesWithNotesModel::preLoadAllNotes()
{
    if (m_documentProvider)
        m_documentProvider->loadAllPages();
}

void PagesWithNotesModel::_addPage(int pageIndex)
{
    if (m_documentProvider == nullptr)
        return;

    auto page = m_documentProvider->loadPage(pageIndex);
    if (!page)
        return;

    if (m_notesModels.contains(pageIndex) && m_notesModels.value(pageIndex) != nullptr) {
        auto notesModel = m_notesModels.value(pageIndex);
        notesModel->setPageSource(page);
    } else {
        auto notesModel = new NotesModel(this);
        if (notesModel == nullptr)
            return;

        connect(notesModel, &NotesModel::loadingChanged, this, [this, pageIndex](bool loading) {
            Q_UNUSED(loading)
            emit dataChanged(index(pageIndex), index(pageIndex), { LoadingRole, CountRole });
        });

        notesModel->setPageSource(page);
        m_notesModels.insert(pageIndex, notesModel);
    }

    emit dataChanged(index(pageIndex), index(pageIndex), { LoadingRole });
}
