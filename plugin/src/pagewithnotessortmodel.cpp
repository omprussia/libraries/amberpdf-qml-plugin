// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "pagewithnotessortmodel.h"

#include "notesmodel.h"
#include "pageswithnotesmodel.h"
#include "pagewithnotessortmodel.h"

PageWithNotesSortModel::PageWithNotesSortModel(QObject *parent) : QSortFilterProxyModel(parent)
{
    this->setFilterRole(PagesWithNotesModel::CountRole);
    setFilterParameter(FilterParameter::None);
}

PagesWithNotesModel *PageWithNotesSortModel::sourceModel() const
{
    return static_cast<PagesWithNotesModel *>(QSortFilterProxyModel::sourceModel());
}

int PageWithNotesSortModel::filterParameter() const
{
    return m_filterParameter;
}

bool PageWithNotesSortModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    if (m_filterParameter == FilterParameter::None)
        return true;

    if (this->sourceModel() == nullptr)
        return false;

    auto index = this->sourceModel()->index(source_row, 0, source_parent);
    if (index.isValid()) {
        auto valueRole = index.data(PagesWithNotesModel::CountRole);
        if (valueRole.isValid()) {
            bool ok(false);
            auto value = valueRole.toInt(&ok);
            if (ok && value > 0)
                return true;
        }
    }

    return false;
}

void PageWithNotesSortModel::setSourceModel(PagesWithNotesModel *sourceModel)
{
    QSortFilterProxyModel::setSourceModel(static_cast<QAbstractItemModel*>(sourceModel));
}

void PageWithNotesSortModel::setFilterParameter(int filterParametr)
{
    if (m_filterParameter == filterParametr)
        return;

    m_filterParameter = filterParametr;

    invalidateFilter();

    emit filterParameterChanged(m_filterParameter);
}
