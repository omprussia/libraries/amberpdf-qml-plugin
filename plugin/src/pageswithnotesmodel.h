// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PAGESWITHNOTESMODEL_H
#define PAGESWITHNOTESMODEL_H

#include <QAbstractListModel>

class BasePdfDocument;
class NotesModel;
class PagesWithNotesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum PageRoles
    {
        LoadingRole = Qt::UserRole + 1,
        NotesModelRole,
        CountRole,
        PageIndexRole
    };

    explicit PagesWithNotesModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    void setNewData(BasePdfDocument *newData);

public slots:
    void preLoadAllNotes();

private slots:
    void _addPage(int pageIndex);

private:
    BasePdfDocument *m_documentProvider = nullptr;
    QMap<int, NotesModel *> m_notesModels;
};

#endif // PAGESWITHNOTESMODEL_H
