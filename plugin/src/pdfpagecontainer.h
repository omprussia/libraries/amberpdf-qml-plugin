// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PDFPAGECONTAINER_H
#define PDFPAGECONTAINER_H

#include <QQuickItem>

#include "pdfviewtypes.h"

class BasePage;
class BaseAnnotation;
class DocumentMapper;
class QSGTexture;
class PdfPageTile;
class PdfAnnotation;
class PdfSimpleAnnotation;
class PdfSimpleNote;
class PdfBackgroundPage;
class PdfPageContainer : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QSizeF requestedSize READ requestedSize WRITE setRequestedSize NOTIFY requestedSizeChanged)
    Q_PROPERTY(Qt::Orientation orientation READ orientation WRITE setOrientation NOTIFY orientationChanged)
    Q_PROPERTY(qreal scale READ scale WRITE setScale NOTIFY scaleChanged)
    Q_PROPERTY(bool annotationsPaint READ annotationsPaint WRITE setAnnotationsPaint NOTIFY annotationsPaintChanged)
    Q_PROPERTY(bool notesPaint READ notesPaint WRITE setNotesPaint NOTIFY notesPaintChanged)
    Q_PROPERTY(bool checked READ checked WRITE setChecked NOTIFY checkedChanged)

public:
    explicit PdfPageContainer(QQuickItem *parent = nullptr);
    ~PdfPageContainer() override;

    QSGNode *updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *) override;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;

    QSizeF requestedSize() const;
    Qt::Orientation orientation() const;
    qreal scale() const;
    bool checked() const;
    void setPageSource(QSharedPointer<BasePage> pageSource);
    void setPageGeometry(const PageGeometry &pg);
    int index() const;
    void setMapper(DocumentMapper *mapper);
    static QSizeF pageCurrentSize(const PageGeometry &pageGeometry, const QSizeF &requestedSize, Qt::Orientation orientation, qreal scale);
    void setVisibleArea(const QRectF &visibleArea);
    bool annotationsPaint() const;
    bool notesPaint() const;
    bool grayScaleRendering() const;
    QSharedPointer<BasePage> source() const;
    void addAnnotation(const QRectF &rect, const QColor &color,
                       const QString &author, const QString &content);
    void removeNote(int noteId);
    void editNote(int noteId, const QString &newContent, const QColor &newColor);

public slots:
    void setRequestedSize(QSizeF requestedSize);
    void setOrientation(Qt::Orientation orientation);
    void setScale(qreal scale);
    void setAnnotationsPaint(bool annotationsPaint);
    void setNotesPaint(bool notesPaint);
    void setGrayScaleRendering(bool grayScaleRendering);
    void setChecked(bool checked);
    void setIndex(int pageIndex);
    void setCentering(bool centering);
    void setFitToPage(bool fitToPage);

signals:
    void requestedSizeChanged(QSizeF requestedSize);
    void orientationChanged(Qt::Orientation orientation);
    void scaleChanged(qreal scale);
    void pageReady();
    void pageChanged();
    void annotationsPaintChanged(bool annotationsPaint);
    void annotationsLoaded();
    void annotationActivate(BaseAnnotation *);
    void notesPaintChanged(bool notesPaint);
    void noteActivate(QString, QString, int);
    void noteRemoved(int, bool);
    void noteEdited(int, bool);
    void checkedChanged(bool checked);
    void sendCheckChangedInfo(int index, bool checked);

private slots:
    void _correctSize();
    void _tailorise();
    void _updateVisible();
    void _loadAnnotations();
    void _prepareBackgroundPage();
    void _noteActivate(QString noteText, QString author);

private:
    PageGeometry m_pageGeometry;
    QSizeF m_requestedSize;
    QSizeF m_pageImageSize;
    Qt::Orientation m_orientation;
    QSharedPointer<BasePage> m_pageSource;
    qreal m_scale;
    qreal m_imageScale;
    DocumentMapper *m_mapper;
    QHash<int, PdfPageTile *> m_tilesMap;
    PdfBackgroundPage *m_backgroundPage;
    qreal m_maxTileZ;
    QRectF m_visibleArea;
    quint16 m_tileSize;
    bool m_annotationsPaint;
    bool m_notesPaint;
    bool m_allTilesReady;
    QList<PdfSimpleAnnotation *> m_annotationsItems;
    QList<PdfSimpleNote *> m_notesItems;
    bool m_grayScaleRendering;
    bool m_checked;
    int m_pageIndex;
    bool m_centering;
    bool m_fitToPage;
};

#endif // PDFPAGECONTAINER_H
