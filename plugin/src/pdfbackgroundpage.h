// SPDX-FileCopyrightText: 2023-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#pragma once

#include <QQuickItem>
#include <QImage>
#include <QPointer>

class BasePage;
class QTimer;
class QSGTexture;
class QPropertyAnimation;
class BitmapLoaderWorker;
class PdfBackgroundPage : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(bool renderable READ renderable WRITE setRenderable NOTIFY renderableChanged)

public:
    explicit PdfBackgroundPage(QQuickItem *parent = nullptr);
     ~PdfBackgroundPage() override;

    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;

    void setPageSource(QSharedPointer<BasePage> pageSource);
    void setActualPageSize(const QSizeF &actualPageSize);
    bool renderable() const;
    bool isBitmap() const;

public slots:
    void setImageScale(qreal imageScale);
    void setPageScale(qreal pageScale);
    void setCentering(bool centering);
    void setFitToPage(bool fitToPage);
    void render(bool force = false);
    void setRenderable(bool renderable);

signals:
    void bitmapError();
    void partReady();
    void animationEnded();
    void renderableChanged(bool renderable);
    void stopRender();

private slots:
    void _loadBitmap();
    void _clearImage();

private:
    QImage m_pagePart;
    QSharedPointer<BasePage> m_pageSource;
    QSizeF m_actualPageSize;
    bool m_needUpdateImage;
    bool m_needClearImage;
    bool m_renderInProcess;
    bool m_renderable;
    bool m_forceRender;
    QTimer *m_timer;
    QTimer *m_timerClear;
    qreal m_imageScale;
    qreal m_pageScale;
    bool m_centering;
    bool m_fitToPage;
    QPropertyAnimation *m_animation;
    QPointer<BitmapLoaderWorker> m_loader;
};
