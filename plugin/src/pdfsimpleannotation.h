// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PDFSIMPLEANNOTATION_H
#define PDFSIMPLEANNOTATION_H

#include <QQuickItem>
#include <QImage>

class QSGTexture;
class BaseAnnotation;
class PdfSimpleAnnotation : public QQuickItem
{
    Q_OBJECT

public:
    explicit PdfSimpleAnnotation(QQuickItem *parent = nullptr, BaseAnnotation *source = nullptr);

    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;

    BaseAnnotation *source() const;

public slots:
    bool event(QEvent *event) override;
    void clearHighlight();

signals:
    void triggered(BaseAnnotation *);

private:
    void _setHighlight(bool highlight);

private:
    BaseAnnotation *m_annotationSource;
    QImage m_paper;
    bool m_highlighted;
    bool m_needUpdateImage;
};


#endif // PDFSIMPLEANNOTATION_H
