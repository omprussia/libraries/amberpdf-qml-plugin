// SPDX-FileCopyrightText: 2022-2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QQuickWindow>
#include <QSGSimpleTextureNode>

#include "baseannotation.h"

#include "pdfsimplenote.h"

PdfSimpleNote::PdfSimpleNote(QQuickItem *parent, BaseAnnotation *source) : QQuickItem(parent),
    m_highlighted(false),
    m_needUpdateImage(false),
    m_noteSource(source)
{
    setFlag(QQuickItem::ItemHasContents, true);
    setAcceptedMouseButtons(Qt::AllButtons);

    connect(this, &PdfSimpleNote::xChanged, this, &PdfSimpleNote::update);
    connect(this, &PdfSimpleNote::yChanged, this, &PdfSimpleNote::update);
    connect(this, &PdfSimpleNote::widthChanged, this, &PdfSimpleNote::update);
    connect(this, &PdfSimpleNote::heightChanged, this, &PdfSimpleNote::update);

    connect(parent, &QQuickItem::yChanged, this, &PdfSimpleNote::clearHighlight);
    connect(parent, &QQuickItem::xChanged, this, &PdfSimpleNote::clearHighlight);

    m_paper = QImage(1, 1, QImage::Format_RGBA8888);
    auto fillColor = m_noteSource->color.isValid()
            ? m_noteSource->color
            : qRgba(21, 222, 225, HIGHLIGHT_OPACITY_NOTE_OFF);
    fillColor.setAlpha(HIGHLIGHT_OPACITY_NOTE_OFF);
    m_paper.fill(fillColor);
}

QSGNode *PdfSimpleNote::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    if (height() <= 0 || width() <= 0)
        return nullptr;

    auto node = static_cast<QSGSimpleTextureNode *>(oldNode);

    if (node == nullptr) {
        node = new QSGSimpleTextureNode();
        node->setOwnsTexture(true);
    }

    if (m_needUpdateImage || node->texture() == nullptr) {
        if (node->texture() != nullptr)
            node->texture()->deleteLater();

        node->setTexture(window()->createTextureFromImage(m_paper));

        m_needUpdateImage = false;
    }

    node->setRect(boundingRect());

    return node;
}

BaseAnnotation *PdfSimpleNote::source() const
{
    return m_noteSource;
}

bool PdfSimpleNote::event(QEvent *event)
{
    if (opacity() < 1.0f)
        return QQuickItem::event(event);

    switch (event->type()) {
    case QEvent::MouseButtonPress:
        _setHighlight(true);
        return true;

    case QEvent::MouseButtonRelease:
        if (m_noteSource != nullptr)
            emit triggered(m_noteSource->content, m_noteSource->author);
        _setHighlight(false);
        break;

    case QEvent::MouseMove:
        _setHighlight(false);
        break;

    default:
        break;
    }

    return QQuickItem::event(event);
}

void PdfSimpleNote::_setHighlight(bool highlight)
{
    if (!m_noteSource)
        return;

    if (m_highlighted != highlight) {
        m_highlighted = highlight;
        m_needUpdateImage = true;
        auto fillColor = m_noteSource->color.isValid()
                ? m_noteSource->color
                : qRgba(21, 222, 225, HIGHLIGHT_OPACITY_NOTE_OFF);
        fillColor.setAlpha(highlight ? HIGHLIGHT_OPACITY_NOTE_ON : HIGHLIGHT_OPACITY_NOTE_OFF);
        m_paper.fill(fillColor);
        update();
    }
}

void PdfSimpleNote::clearHighlight()
{
   _setHighlight(false);
}
