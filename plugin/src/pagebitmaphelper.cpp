// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "pagebitmaphelper.h"

namespace bitmaphelper {

QPointF bias(const QPointF &startPosition,
             const QSizeF &originalPageSize,
             const QSizeF &actualPageSize,
             qreal imageScale,
             bool centering,
             bool fitToPage)
{
    auto deltaY = actualPageSize.height() != originalPageSize.height()
                      ? (actualPageSize.height() - originalPageSize.height()) / 2
                      : 0;
    auto deltaX = actualPageSize.width() != originalPageSize.width()
                      ? (actualPageSize.width() - originalPageSize.width()) / 2
                      : 0;

    auto biasX = centering ? startPosition.x() + (deltaX * imageScale) : startPosition.x();
    auto biasY = fitToPage ? startPosition.y() + deltaY * imageScale
                           : (deltaY >= 0 ? startPosition.y()
                                          : startPosition.y()
                                                - (((originalPageSize.height() + deltaY)
                                                    - originalPageSize.width())
                                                   / 2)
                                                      * imageScale);

    return QPointF(biasX, biasY);
}

} // namespace bitmaphelper
