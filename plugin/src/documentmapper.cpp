// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "basedocument.h"

#include "documentmapper.h"

#include <QDebug>

namespace {

using SpecialRangesType = std::map<int, int>;
using SpecialRangesIterator = std::map<int, int>::iterator;

void removeFromBegin(SpecialRangesIterator it,
                     std::set<int> &specialIndexes,
                     SpecialRangesType &specialRangeIndexes)
{
    if (it->second - it->first == 1) {
        specialIndexes.insert(it->second);
        specialRangeIndexes.erase(it);
    } else {
        specialRangeIndexes[it->first + 1] = it->second;
        specialRangeIndexes.erase(it);
    }
}

void removeFromEnd(SpecialRangesIterator it,
                   std::set<int> &specialIndexes,
                   SpecialRangesType &specialRangeIndexes)
{
    if (it->second - it->first == 1) {
        specialIndexes.insert(it->first);
        specialRangeIndexes.erase(it);
    } else {
        specialRangeIndexes[it->first] = it->second - 1;
    }
}

void removeFromMiddle(int index,
                      SpecialRangesIterator it,
                      std::set<int> &specialIndexes,
                      SpecialRangesType &specialRangeIndexes)
{
    if (index - it->first == 1 && it->second - index == 1) {
        specialIndexes.insert(it->first);
        specialIndexes.insert(it->second);
        specialRangeIndexes.erase(it);
    } else {
        auto prevRange = *it;
        if (index - it->first == 1) {
            specialIndexes.insert(it->first);
            specialRangeIndexes[index + 1] = prevRange.second;
            auto removingIt = std::find_if(specialRangeIndexes.begin(),
                                           specialRangeIndexes.end(),
                                           [prevRange](const std::pair<int, int> &t) -> bool {
                                               return prevRange.first == t.first
                                                      && prevRange.second == t.second;
                                           });
            if (removingIt != specialRangeIndexes.end())
                specialRangeIndexes.erase(removingIt);
            return;
        }

        if (it->second - index == 1) {
            specialIndexes.insert(it->second);
            specialRangeIndexes[prevRange.first] = index - 1;
            auto removingIt = std::find_if(specialRangeIndexes.begin(),
                                           specialRangeIndexes.end(),
                                           [prevRange](const std::pair<int, int> &t) -> bool {
                                               return prevRange.first == t.first
                                                      && prevRange.second == t.second;
                                           });
            if (removingIt != specialRangeIndexes.end())
                specialRangeIndexes.erase(removingIt);
            return;
        }

        specialRangeIndexes[prevRange.first] = index - 1;
        specialRangeIndexes[index + 1] = prevRange.second;
    }
}

QSizeF pageToAspectRatio(const QSizeF &pageSize, qreal pageAspectRatio, bool fitToPage)
{
    if (pageSize.width() == 0 || pageSize.height() == 0) {
        qCritical() << "Invalid page size: width=" << pageSize.width()
                    << ", height=" << pageSize.height();
        return pageSize;
    }

    const auto scaleCoeff = (pageSize.height() * pageAspectRatio) / pageSize.width();
    const auto originalPageAspectRatio = pageSize.width() / pageSize.height();

    if (pageAspectRatio < 1.0 && originalPageAspectRatio > 1.0)
        return QSizeF(pageSize.width(), pageSize.height() * (1 / scaleCoeff));

    if (fitToPage)
        return QSizeF(pageSize.width() * scaleCoeff, pageSize.height());

    if (pageAspectRatio > 1.0 && originalPageAspectRatio > 1.0)
        return QSizeF(pageSize.width(), pageSize.height());

    const auto pageHeight = pageSize.height() - (pageSize.width() * scaleCoeff - pageSize.height()) / 2;
    const auto pageWidth = pageHeight * pageAspectRatio;
    return QSizeF(pageWidth, pageHeight);
}
}

DocumentMapper::DocumentMapper(QQuickItem *parent)
    : QQuickItem(parent)
    , m_orientation(Qt::Vertical)
    , m_spacing(SPACING_DEFAULT_VALUE)
    , m_contentHeight(0.0)
    , m_contentWidth(0.0)
    , m_lastPageActualSize(0.0)
    , m_pageAspectRatio(1.0)
    , m_reverse(false)
    , m_showAll(false)
    , m_specialPagesCount(0)
    , m_fitToPage(true)
{
    connect(this, &DocumentMapper::widthChanged, this, &DocumentMapper::_mapPages);
    connect(this, &DocumentMapper::heightChanged, this, &DocumentMapper::_mapPages);
    connect(parentItem(), &QQuickItem::widthChanged, this, &DocumentMapper::_updateSize);
    connect(parentItem(), &QQuickItem::heightChanged, this, &DocumentMapper::_updateSize);
}

QSGNode *DocumentMapper::updatePaintNode(QSGNode *, QQuickItem::UpdatePaintNodeData *)
{
    return nullptr;
}

QHash<int, PagePosition> DocumentMapper::actualMap() const
{
    return m_actualPagesCoordinates;
}

QPointF DocumentMapper::pagePosition(int pageIndex) const
{
    if (pageIndex < 0 || pageIndex > m_originalPagesMap.pages.size())
        return {  };

    auto pagePosition = m_originalPagesMap.pages.at(pageIndex);
    return { static_cast<qreal>(pagePosition.startX), static_cast<qreal>(pagePosition.startY)};
}

qreal DocumentMapper::contentHeight() const
{
    return m_contentHeight;
}

qreal DocumentMapper::contentWidth() const
{
    return m_contentWidth;
}

qreal DocumentMapper::spacing() const
{
    return m_spacing;
}

qreal DocumentMapper::lastPageActualSize() const
{
    return m_lastPageActualSize;
}

void DocumentMapper::forceUpdate()
{
    _updateSize();
    _mapPages();
}

void DocumentMapper::setDocumentProvider(BasePdfDocument *documentProvider)
{
    if (documentProvider == nullptr)
        return;

    if (m_documentProvider == documentProvider)
        return;

    m_documentProvider = documentProvider;

    const auto pageCount = m_documentProvider->count();
    m_originalPagesMap.pages.clear();
    m_originalPagesMap.pages.reserve(pageCount);

    if (pageCount < 1)
        m_spacing = 0;

    qreal originalPageStartX = 0.0;
    qreal originalPageStartY = 0.0;

    const bool useOriginalSizes = qFuzzyCompare(static_cast<double>(m_pageAspectRatio),
                                          static_cast<double>(1.0));

    for (int i = 0; i < pageCount; ++i) {

        auto pageSize = m_documentProvider->pageSize(i);

        if (!useOriginalSizes)
            pageSize = pageToAspectRatio(m_documentProvider->pageSize(i),
                                         m_pageAspectRatio,
                                         m_fitToPage);

        m_originalPagesMap.pages.append({originalPageStartX,
                                         originalPageStartY,
                                         originalPageStartX + pageSize.width(),
                                         originalPageStartY + pageSize.height()});
        originalPageStartX += pageSize.width();
        originalPageStartY += pageSize.height();
    }

    _mapPages();
}

void DocumentMapper::setOrientation(Qt::Orientation orientation)
{
    if (m_orientation == orientation)
        return;

    m_orientation = orientation;
    _mapPages();
}

void DocumentMapper::setSpecialPageIndexes(const std::map<int, int> &specialRangeIndexes,
                                           const std::set<int> &specialIndexes)
{
    m_specialRangeIndexes = specialRangeIndexes;
    m_specialIndexes = specialIndexes;

    _mapPages();
}

void DocumentMapper::setPageAspectRatio(qreal pageAspectRatio)
{
    if (qFuzzyCompare(static_cast<double>(m_pageAspectRatio), static_cast<double>(pageAspectRatio)))
        return;

    m_pageAspectRatio = pageAspectRatio;
    if (!m_originalPagesMap.pages.empty())
        _mapPages();
}

void DocumentMapper::setReverse(bool reverse)
{
    if (m_reverse == reverse)
        return;

    m_reverse = reverse;
    _mapPages();
}

void DocumentMapper::setSpacing(qreal spacing)
{
    if (qFuzzyCompare(m_spacing, spacing))
        return;

    m_spacing = spacing;
    if (!m_originalPagesMap.pages.empty())
        _mapPages();
}

void DocumentMapper::setFitToPage(bool fitToPage)
{
    if (m_fitToPage == fitToPage)
        return;

    m_fitToPage = fitToPage;

    if (!m_originalPagesMap.pages.empty())
        _mapPages();
}

void DocumentMapper::setShowAll(bool showAll)
{
    if (m_showAll == showAll)
        return;

    m_showAll = showAll;
    _mapPages();
}

void DocumentMapper::_mapPages()
{
    if (width() <= 0 || height() <= 0)
        return;

    if (m_documentProvider == nullptr)
        return;

    int pageIndex = 0;
    auto contentHeight = -1.0f;
    auto contentWidth = -1.0f;
    PagePosition position;
    int specialPagesCount = 0;

    auto maxPageIndex = m_originalPagesMap.pages.size() - 1;
    if (!m_originalPagesMap.pages.empty() && !m_specialIndexes.empty()
        && m_specialIndexes.find(maxPageIndex) == m_specialIndexes.end()
        && m_originalPagesMap.pages.size() < *m_specialIndexes.rbegin()) {
        m_specialIndexes.insert(maxPageIndex);
    }

    switch (m_orientation) {
    case Qt::Vertical: {
        for (auto &page : m_originalPagesMap.pages) {
            auto inverseIndex = m_reverse ? m_originalPagesMap.pages.size() - 1 - pageIndex : pageIndex;
            if (isHasSpecial() && !isIndexSpecal(inverseIndex)) {
                ++pageIndex;
                continue;
            }

            position.end += width() * page.heightToWidthRatio() + m_spacing;
            m_actualPagesCoordinates.insert(inverseIndex, position);
            m_lastPageActualSize = position.end - position.start - m_spacing;
            position.start = position.end;
            ++pageIndex;
            ++specialPagesCount;
        }

        contentHeight = position.end;
        contentWidth = -1;
        break;
    }
    case Qt::Horizontal : {
        for (auto &page : m_originalPagesMap.pages) {
            auto inverseIndex = m_reverse ? m_originalPagesMap.pages.size() - 1 - pageIndex : pageIndex;
            if (isHasSpecial() && !isIndexSpecal(inverseIndex)) {
                ++pageIndex;
                continue;
            }

            auto fitHeight = qMin(width() * page.heightToWidthRatio(), height());
            position.end += fitHeight / page.heightToWidthRatio() + m_spacing;
            m_actualPagesCoordinates.insert(inverseIndex, position);
            m_lastPageActualSize = position.end - position.start - m_spacing;
            position.start = position.end;
            ++pageIndex;
            ++specialPagesCount;
        }

        contentHeight = - 1;
        contentWidth = position.end;
        break;
    }
    }

    if (!qFuzzyCompare(double(contentHeight), double(m_contentHeight))) {
        m_contentHeight = contentHeight;
        emit contentHeightChanged(m_contentHeight);
    }

    if (!qFuzzyCompare(double(contentWidth), double(m_contentWidth))) {
        m_contentWidth = contentWidth;
        emit contentWidthChanged(m_contentWidth);
    }

    if (specialPagesCount != m_specialPagesCount) {
        m_specialPagesCount = specialPagesCount;
        emit specialPagesCountChanged(m_specialPagesCount);
    }

    emit mapEnd();
}

void DocumentMapper::_updateSize()
{
    if (parentItem() == nullptr)
        return;

    setWidth(parentItem()->width());
    setHeight(parentItem()->height());
}

bool DocumentMapper::isIndexSpecal(int pageIndex) const
{
    auto it = std::find_if(m_specialRangeIndexes.begin(),
                           m_specialRangeIndexes.end(),
                           [pageIndex](const std::pair<int, int> &t) -> bool {
                               return pageIndex >= t.first && pageIndex <= t.second;
                           });

    return m_specialIndexes.find(pageIndex) != m_specialIndexes.end()
           || it != m_specialRangeIndexes.end();
}

bool DocumentMapper::isHasSpecial() const
{
    return m_showAll ? false : !(m_specialRangeIndexes.empty() && m_specialIndexes.empty());
}

int DocumentMapper::minSpecialIndex() const
{
    int firstMin = m_specialIndexes.empty() ? 0 : *m_specialIndexes.begin();

    if (m_specialRangeIndexes.empty())
        return firstMin;

    if (!m_specialIndexes.empty())
        return std::min(firstMin,
                        std::min(m_specialRangeIndexes.begin()->first,
                                 m_originalPagesMap.pages.size() - 1));

    return std::min(m_specialRangeIndexes.begin()->first, m_originalPagesMap.pages.size() - 1);
}

int DocumentMapper::maxSpecialIndex() const
{
    int firstMax = m_specialIndexes.empty()
                       ? 0
                       : std::min(*m_specialIndexes.rbegin(), m_originalPagesMap.pages.size() - 1);

    if (m_specialRangeIndexes.empty())
        return firstMax;

    if (m_specialRangeIndexes.size() == 1)
        return std::max(firstMax,
                        std::min(m_specialRangeIndexes.begin()->second,
                                 m_originalPagesMap.pages.size() - 1));

    auto it = std::max_element(m_specialRangeIndexes.begin(),
                               m_specialRangeIndexes.end(),
                               [](const std::pair<int, int> &a, const std::pair<int, int> &b) {
                                   return a.second < b.second;
                               });

    return std::min(std::max(firstMax, it->second), m_originalPagesMap.pages.size() - 1);
}

void DocumentMapper::removeFromSpecial(int index)
{
    if (index < 0 || index > m_originalPagesMap.pages.size())
        return;

    auto iter = m_specialIndexes.find(index);
    if (iter != m_specialIndexes.end())
        m_specialIndexes.erase(iter);

    auto it = std::find_if(m_specialRangeIndexes.begin(),
                           m_specialRangeIndexes.end(),
                           [index](const std::pair<int, int> &t) -> bool {
                               return index >= t.first && index <= t.second;
                           });

    if (it == m_specialRangeIndexes.end())
        return;

    if (it->first == index) {
        removeFromBegin(it, m_specialIndexes, m_specialRangeIndexes);
        return;
    }

    if (it->second == index) {
        removeFromEnd(it, m_specialIndexes, m_specialRangeIndexes);
        return;
    }

    removeFromMiddle(index, it, m_specialIndexes, m_specialRangeIndexes);
}

void DocumentMapper::insertToSpecial(int index)
{
    if (index < 0 || index > m_originalPagesMap.pages.size())
        return;

    m_specialIndexes.insert(index);
}

qreal DocumentMapper::pageAspectRatio() const
{
    return m_pageAspectRatio;
}

std::set<int> DocumentMapper::specialIndexes() const
{
    return m_specialIndexes;
}

std::map<int, int> DocumentMapper::specialRangeIndexes() const
{
    return m_specialRangeIndexes;
}

int DocumentMapper::specialPagesCount() const
{
    return m_specialPagesCount;
}

bool DocumentMapper::fitToPage() const
{
    return m_fitToPage;
}

PagePosition DocumentMapper::actualPagePosition(int pageIndex) const
{
    if (!m_actualPagesCoordinates.contains(pageIndex))
        return {  };

    return m_actualPagesCoordinates.value(pageIndex);
}

PageGeometry DocumentMapper::originalPageGeometry(int pageIndex) const
{
    if (pageIndex < 0 || pageIndex > m_originalPagesMap.pages.size())
        return {  };

    return m_originalPagesMap.pages.at(pageIndex);
}
