// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QQuickWindow>
#include <QTimer>
#include <QFutureWatcher>
#include <QtConcurrent/QtConcurrent>

#include "basedocument.h"
#include "baseannotation.h"
#include "documentmapper.h"
#include "pdfpagecontainer.h"
#include "bookmarksmodel.h"
#include "pageswithnotesmodel.h"

#include "pdfview.h"

PdfView::PdfView(QQuickItem *parent)
    : QQuickItem(parent)
    , m_count(-1)
    , m_contentHeight(-1)
    , m_contentWidth(-1)
    , m_contentY(-1)
    , m_contentX(-1)
    , m_moveDirection(-1)
    , m_paintedItemsSize(-1)
    , m_currentIndex(-1)
    , m_catchBound(0)
    , m_orientation(Qt::Vertical)
    , m_itemScale(1.0)
    , m_annotationsPaint(false)
    , m_documentProvider(nullptr)
    , m_documentEdited(false)
    , m_pagesWithNotesModel(nullptr)
    , m_pageNumberComponent(nullptr)
    , m_grayScaleRendering(false)
    , m_pageAspectRatio(1.0)
    , m_reverse(false)
    , m_pageCheckComponent(nullptr)
    , m_hiddenPagesOpacity(0.4)
    , m_specialPagesCount(-1)
    , m_pageSpacing(SPACING_DEFAULT_VALUE)
    , m_fitToPage(true)
{
    setFlag(QQuickItem::ItemHasContents, true);
    setFlag(QQuickItem::ItemAcceptsInputMethod, true);
    setAcceptedMouseButtons(Qt::AllButtons);

    m_mapper = new DocumentMapper(this);
    connect(m_mapper, &DocumentMapper::contentWidthChanged, this, &PdfView::_updateContentSize);
    connect(m_mapper, &DocumentMapper::contentHeightChanged, this, &PdfView::_updateContentSize);
    connect(m_mapper, &DocumentMapper::mapEnd, this, &PdfView::_calculateVisible);
    connect(m_mapper,
            &DocumentMapper::specialPagesCountChanged,
            this,
            &PdfView::_updateSpecialPagesCount);
    connect(this, &PdfView::reverseChanged, m_mapper, &DocumentMapper::setReverse);
    connect(this, &PdfView::loadPages, this, &PdfView::_preparePages);
    connect(this, &PdfView::_specialIndexesChanged, this, &PdfView::_updateSpecailIndexes);

    connect(this, &QQuickItem::windowChanged, this, [this]() {
        auto *window = this->window();

        if (window != nullptr)
            connect(window, &QQuickWindow::beforeSynchronizing, this, &PdfView::_calculateVisible, Qt::DirectConnection);
    });

    m_timer = new QTimer(this);
    m_timer->setSingleShot(true);

    emit statusChanged(BasePdfDocument::DocumentStatus::Null);

    m_bookmarksModel = new BookmarksModel(this);
}

PdfView::~PdfView() = default;

QSGNode *PdfView::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    return oldNode;
}

void PdfView::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickItem::geometryChanged(newGeometry, oldGeometry);

    if (qFuzzyCompare(newGeometry.width(), oldGeometry.width())
            && qFuzzyCompare(newGeometry.height(), oldGeometry.height()))
        return;

    if (m_contentWidth <= 0 && m_contentHeight <= 0)
        return;

    for (auto paintItem : m_pages) {
        auto *page = qobject_cast<PdfPageContainer *>(paintItem);
        if (page == nullptr)
            continue;

        page->setRequestedSize({ width(), height() });
    }

    if (!qFuzzyCompare(oldGeometry.width(), newGeometry.width())) {
        emit widthChanged();
        emit lastPageIndentChanged(lastPageIndent());
    }

    if (!qFuzzyCompare(oldGeometry.height(), newGeometry.height())) {
        emit heightChanged();
        emit lastPageIndentChanged(lastPageIndent());

        if (m_timer->isActive()) {
            _positionPages();
            return;
        }

        if (m_orientation == Qt::Vertical) {
            auto newContentY = m_contentY;
            if (m_contentY >= 1.0f)
                newContentY += (oldGeometry.height() - newGeometry.height()) / 2.0f;

            if (m_currentIndex == m_count - 1) {
                auto index  = m_mapper->isHasSpecial() ? (m_reverse ? m_mapper->minSpecialIndex() : m_mapper->maxSpecialIndex()) : m_count - 1;
                auto pagePosition = m_mapper->actualPagePosition(index);
                newContentY = pagePosition.start * m_itemScale - m_catchBound + 1;
            }

            setContentY(newContentY);
        } else {
            auto newContentX = m_contentX;
            if (m_contentX >= 1.0f)
                newContentX += (oldGeometry.width() - newGeometry.width()) / 2.0f;

            if (m_currentIndex == m_count - 1) {
                auto index = m_mapper->isHasSpecial() ? (m_reverse ? m_mapper->minSpecialIndex() : m_mapper->maxSpecialIndex()) : m_count - 1;
                auto pagePosition = m_mapper->actualPagePosition(index);
                newContentX = pagePosition.start * m_itemScale - m_catchBound - 1;
            }

            setContentX(newContentX);
        }

        _positionPages();
    }

    return;
}

int PdfView::count() const
{
    return m_count;
}

qreal PdfView::contentHeight() const
{
    return m_contentHeight;
}

qreal PdfView::contentWidth() const
{
    return m_contentWidth;
}

qreal PdfView::contentY() const
{
    return m_contentY;
}

qreal PdfView::contentX() const
{
    return m_contentX;
}

int PdfView::currentIndex() const
{
    return m_currentIndex;
}

qreal PdfView::catchBound() const
{
    return m_catchBound;
}

Qt::Orientation PdfView::orientation() const
{
    return m_orientation;
}

qreal PdfView::lastPageIndent() const
{
    if (m_orientation == Qt::Vertical)
        return qMax(m_catchBound, height() - m_mapper->lastPageActualSize() / qMax(1.0f, float(m_itemScale)) - m_catchBound);

    if (m_contentWidth <= width())
        return 0;

    return qMax(m_catchBound, width() - m_mapper->lastPageActualSize() / qMax(1.0f, float(m_itemScale)) - m_catchBound);
}

qreal PdfView::itemScale() const
{
    return m_itemScale;
}

bool PdfView::annotationsPaint() const
{
    return m_annotationsPaint;
}

bool PdfView::notesPaint() const
{
    return m_notesPaint;
}

BasePdfDocument *PdfView::documentProvider() const
{
    return m_documentProvider;
}

qreal PdfView::contentTopMargin() const
{
    return m_contentTopMargin;
}

BookmarksModel *PdfView::bookmarksModel() const
{
    return m_bookmarksModel;
}

int PdfView::fileVersion() const
{
    return m_documentProvider == nullptr ? -1 : m_documentProvider->fileVersion();
}

void PdfView::saveDocumentAs(const QString &path)
{
    auto *watcher = new QFutureWatcher<bool>();
    connect(watcher, &QFutureWatcher<bool>::finished, this, [this, watcher]() {
        if (watcher == nullptr) {
            emit documentSaved(false);
            return;
        }

        if (watcher->isFinished() && !watcher->isCanceled()) {
            emit documentSaved(watcher->result());
        } else {
            emit documentSaved(false);
        }

        if (watcher != nullptr)
            watcher->deleteLater();
    });

    watcher->setFuture(QtConcurrent::run(QThreadPool::globalInstance(), [this, path]() {
        return m_documentProvider ? m_documentProvider->saveDocumentAs(path) : false;
    }));
}

void PdfView::addAnnotation(const QRectF &rect, const QColor &color, const QString &author, const QString &content)
{
    for (const auto &page : m_paintedPages) {
        auto pagePosition = m_mapper->actualPagePosition(page);
        if (m_orientation == Qt::Vertical) {
            if (rect.y() >= pagePosition.start && rect.y() <= pagePosition.end) {
                if (!m_pages.contains(page))
                    return;

                QRectF localRect(rect);
                localRect.setY(rect.y() - pagePosition.start);
                localRect.setHeight(rect.height());
                localRect.setWidth(rect.width());

                auto pageContainer = qobject_cast<PdfPageContainer *>(m_pages.value(page));
                pageContainer->addAnnotation(localRect, color, author, content);
                return;
            }
        } else {
            if (rect.x() > pagePosition.start && rect.x() <= pagePosition.end) {
                if (!m_pages.contains(page))
                    return;

                QRectF localRect(rect);
                localRect.setX(rect.x() - pagePosition.start);
                localRect.setHeight(rect.height());
                localRect.setWidth(rect.width());

                auto pageContainer = qobject_cast<PdfPageContainer *>(m_pages.value(page));
                pageContainer->addAnnotation(localRect, color, author, content);
                return;
            }
        }
    }
}

void PdfView::removeNote(int pageId, int noteId)
{
    if (!m_paintedPages.contains(pageId))
        return;

    auto page = qobject_cast<PdfPageContainer *>(m_pages.value(pageId));
    page->removeNote(noteId);
}

void PdfView::editNote(int pageId, int noteId, const QString &newContent, const QColor &newColor)
{
    if (!m_paintedPages.contains(pageId))
        return;

    auto page = qobject_cast<PdfPageContainer *>(m_pages.value(pageId));
    page->editNote(noteId, newContent, newColor);
}

bool PdfView::documentEdited() const
{
    return m_documentEdited;
}

PagesWithNotesModel *PdfView::pagesWithNotesModel() const
{
    return m_pagesWithNotesModel;
}

QQmlComponent *PdfView::pageNumberComponent() const
{
    return m_pageNumberComponent;
}

bool PdfView::reverse() const
{
    return m_reverse;
}

QQmlComponent *PdfView::pageCheckComponent() const
{
    return m_pageCheckComponent;
}

qreal PdfView::hiddenPagesOpacity() const
{
    return m_hiddenPagesOpacity;
}

int PdfView::specialPagesCount() const
{
    return m_specialPagesCount;
}

int PdfView::maxPageIndex() const
{
    return m_mapper->isHasSpecial() ? m_mapper->maxSpecialIndex() : m_count - 1;
}

int PdfView::minPageIndex() const
{
    return m_mapper->isHasSpecial() ? m_mapper->minSpecialIndex() : 0;
}

bool PdfView::fitToPage() const
{
    return m_fitToPage;
}

bool PdfView::centering() const
{
    return m_centering;
}

qreal PdfView::pageSpacing() const
{
    return m_pageSpacing;
}

QString PdfView::pdfPath() const
{
    return m_pdfPath;
}

QString PdfView::pageRange() const
{
    return m_pageRange;
}

bool PdfView::grayScaleRendering() const
{
    return m_grayScaleRendering;
}

qreal PdfView::pageAspectRatio() const
{
    return m_pageAspectRatio;
}

BasePdfDocument::DocumentStatus PdfView::status() const
{
    return m_documentProvider == nullptr ? BasePdfDocument::DocumentStatus::Null
                                         : m_documentProvider->status();
}

void PdfView::setContentY(qreal contentY)
{
    if (qFuzzyCompare(double(m_contentY), double(contentY)))
        return;

    if (m_orientation == Qt::Vertical)
        m_moveDirection = m_contentY - contentY;

    m_contentY = contentY;
    emit contentYChanged(m_contentY);

    _positionPages();
}

void PdfView::setContentX(qreal contentX)
{
    if (qFuzzyCompare(double(m_contentX), double(contentX)))
        return;

    if (m_orientation == Qt::Vertical)
        m_moveDirection = m_contentX - contentX;

    m_contentX = contentX;
    emit contentXChanged(m_contentX);

    _positionPages();
}

void PdfView::setCatchBound(qreal catchBound)
{
    if (qFuzzyCompare(m_catchBound, catchBound))
        return;

    m_catchBound = catchBound;
    emit catchBoundChanged(m_catchBound);
    emit lastPageIndentChanged(lastPageIndent());

    _positionPages();
}

void PdfView::positionViewAtIndex(int index)
{

    if (index < 0 || index > m_count)
        return;

    if (m_reverse && (index == 0 || index == m_count - 1))
        return;

    if (m_mapper->isHasSpecial()) {
        if (index == 0)
            index = m_mapper->minSpecialIndex();

        if (index == m_count - 1)
            index = m_mapper->maxSpecialIndex();
    }

    if (m_currentIndex != index) {
        m_currentIndex = index;
        emit currentIndexChanged(m_currentIndex);
    }

    if (!m_paintedPages.contains(index)) {
        m_paintedPages.clear();
        m_paintedPages.append(index);
    }

    auto pagePosition = m_mapper->actualPagePosition(index);

    if (m_orientation == Qt::Vertical)
        setContentY(pagePosition.start * m_itemScale - m_catchBound + 1);
    else
        setContentX(pagePosition.start * m_itemScale);

    emit contentChanged();

    m_timer->start(1300);
}

void PdfView::setOrientation(Qt::Orientation orientation)
{
    if (m_orientation == orientation)
        return;

    auto currentIndex = m_currentIndex;
    m_orientation = orientation;
    m_mapper->setOrientation(m_orientation);

    positionViewAtIndex(currentIndex);
    emit orientationChanged(m_orientation);
    _positionPages();
}

void PdfView::setItemScale(qreal itemScale)
{

    if (qFuzzyCompare(m_itemScale, itemScale))
        return;

    m_itemScale = itemScale;

    _positionPages();

    emit itemScaleChanged(m_itemScale);
    emit lastPageIndentChanged(lastPageIndent());
}

void PdfView::scaleAroundPoint(const QPointF &center, qreal newScale)
{
    m_contentX -= center.x() - (center.x() / (m_contentWidth * m_itemScale)) * (m_contentWidth * newScale);
    m_contentY -= center.y() - (center.y() / (m_contentHeight * m_itemScale)) * (m_contentHeight * newScale);
    m_itemScale = newScale;

    _positionPages();

    emit contentXChanged(m_contentX);
    emit contentYChanged(m_contentY);
    emit itemScaleChanged(m_itemScale);
}

void PdfView::clicked(const QPointF &point)
{
    if (!m_pageCheckComponent)
        return;

    for (auto pageIndex : m_paintedPages) {
        auto page = m_pages.value(pageIndex);
        if (page == nullptr)
            continue;

        auto pagePosition = m_mapper->actualPagePosition(pageIndex);

        bool horizonatalPageOutside = point.x() > page->x() + page->width()
                                      || point.x() < page->x();
        bool verticalPageOutside = point.y() > pagePosition.end * m_itemScale
                                   || point.y() < pagePosition.start * m_itemScale;

        if (horizonatalPageOutside || verticalPageOutside)
            continue;

        bool isCheck = !(m_mapper->isIndexSpecal(pageIndex));
        auto pageContainer = qobject_cast<PdfPageContainer *>(page);
        pageContainer->setOpacity(isCheck ? 1.0 : m_hiddenPagesOpacity);
        pageContainer->setChecked(isCheck);
    }
}

void PdfView::setAnnotationsPaint(bool annotationsPaint)
{
    if (m_annotationsPaint == annotationsPaint)
        return;

    m_annotationsPaint = annotationsPaint;
    emit annotationsPaintChanged(m_annotationsPaint);
}

void PdfView::setNotesPaint(bool notesPaint)
{
    if (m_notesPaint == notesPaint)
        return;

    m_notesPaint = notesPaint;
    emit notesPaintChanged(m_notesPaint);
}

void PdfView::setDocumentProvider(BasePdfDocument *documentProvider)
{
    if (m_documentProvider == documentProvider)
        return;

    if (documentProvider == nullptr)
        return;

    m_documentProvider = documentProvider;
    emit documentProviderChanged(m_documentProvider);

    m_absolutePosition.setX(0);
    m_absolutePosition.setY(0);

    m_count = -1;
    emit countChanged(m_count);

    m_specialPagesCount = -1;
    emit specialPagesCountChanged(m_specialPagesCount);

    m_contentWidth = -1;
    emit contentWidthChanged(m_contentWidth);

    m_contentHeight = -1;
    emit contentHeightChanged(m_contentHeight);

    emit statusChanged(BasePdfDocument::DocumentStatus::Null);

    if (m_pagesWithNotesModel != nullptr) {
        m_pagesWithNotesModel->deleteLater();
        m_pagesWithNotesModel = nullptr;
    }

    m_pagesWithNotesModel = new PagesWithNotesModel(this);
    emit pagesWithNotesModelChanged(m_pagesWithNotesModel);

    connect(m_documentProvider,
            &BasePdfDocument::pageLoaded,
            this,
            [&](int pageIndex, BasePdfDocument::PageLoadStatus loadStatus) {
                if (!m_pages.contains(pageIndex))
                    return;

                auto page = qobject_cast<PdfPageContainer *>(m_pages.value(pageIndex));
                if (page == nullptr)
                    return;

                if (!page->source() || loadStatus != BasePdfDocument::PageLoadStatus::Success) {
                    auto baseDocument = qobject_cast<BasePdfDocument *>(m_documentProvider);
                    page->setPageSource(baseDocument->loadPage(pageIndex));
                }
            });

    connect(m_documentProvider, &BasePdfDocument::bookmarksLoaded, this, [this]() {
        m_bookmarksModel->setNewData(m_documentProvider->bookmarks());
        emit bookmarksModelChanged(m_bookmarksModel);
    });

    connect(m_documentProvider,
            &BasePdfDocument::statusChanged,
            this,
            [this](BasePdfDocument::DocumentStatus status) {
                if (status == BasePdfDocument::DocumentStatus::Ready) {
                    m_mapper->setSpacing(m_pageSpacing);
                    m_mapper->setPageAspectRatio(m_pageAspectRatio);
                    m_mapper->setFitToPage(m_fitToPage);
                    m_mapper->setDocumentProvider(m_documentProvider);
                    m_count = m_documentProvider->count();
                    if (m_pageNumberComponent) {
                        std::set<int> pageIndexes;
                        std::map<int, int> indexIntervals;
                        if (m_count > 1)
                            indexIntervals = {std::pair(0, m_count - 1)};
                        else
                            pageIndexes = {m_count - 1};

                        if (m_pageRange.isEmpty())
                            m_mapper->setSpecialPageIndexes(indexIntervals, pageIndexes);
                    }
                    m_specialPagesCount = m_mapper->specialPagesCount();
                    m_documentProvider->startLoadBookmarks();
                    m_pagesWithNotesModel->setNewData(m_documentProvider);
                    emit countChanged(m_count);
                    emit specialPagesCountChanged(m_specialPagesCount);
                    emit maxPageIndexChanged(m_mapper->maxSpecialIndex());
                    emit minPageIndexChanged(m_mapper->minSpecialIndex());
                }

                _calculateVisible();
                emit statusChanged(status);
            });

    connect(m_documentProvider, &BasePdfDocument::fileVersionChanged, this, [this](int fileVersion) {
        if (fileVersion > 0)
            emit fileVersionChanged(m_documentProvider->fileVersion());
    });

    m_mapper->setWidth(width());
    m_mapper->setHeight(height());

    _loadDocument();
}

void PdfView::setPdfPath(QString pdfPath)
{
    if (m_pdfPath == pdfPath)
        return;

    m_pdfPath = pdfPath;
    emit pdfPathChanged(m_pdfPath);

    _loadDocument();
}

void PdfView::setPageRange(const QString &pageRange)
{
    if (m_pageRange == pageRange)
        return;

    m_pageRange = pageRange;
    if (m_pageRange.isEmpty())
        return;

    std::map<int, int> indexIntervals;
    std::set<int> pageIndexes;
    QStringList ranges = pageRange.split(',');

    for (const auto &range : ranges) {
        QStringList rangeInterval = range.split('-');
        if (rangeInterval.size() == 2) {
            auto rangeStart = std::min(rangeInterval[0].toInt() - 1, rangeInterval[1].toInt() - 1);
            auto rangeEnd = std::max(rangeInterval[0].toInt() - 1, rangeInterval[1].toInt() - 1);

            auto it = indexIntervals.find(rangeStart);
            if (it == indexIntervals.end()) {
                indexIntervals[rangeStart] = rangeEnd;
            } else {
                indexIntervals[rangeStart] = std::max(rangeEnd, it->second);
            }
        } else {
            if (rangeInterval.size() == 1)
                pageIndexes.insert(rangeInterval[0].toInt() - 1);
        }
    }
    m_mapper->setSpecialPageIndexes(indexIntervals, pageIndexes);
}

void PdfView::setPageNumberComponent(QQmlComponent *pageNumberComponent)
{
    if (m_pageNumberComponent == pageNumberComponent)
        return;

    m_pageNumberComponent = pageNumberComponent;
    emit pageNumberComponentChanged(m_pageNumberComponent);
}

void PdfView::setGrayScaleRendering(bool grayScaleRendering)
{
    if (m_grayScaleRendering == grayScaleRendering)
        return;

    m_grayScaleRendering = grayScaleRendering;
    emit grayScaleRenderingChanged(m_grayScaleRendering);
}

void PdfView::setPageAspectRatio(qreal pageAspectRatio)
{
    if (qFuzzyCompare(static_cast<double>(m_pageAspectRatio), static_cast<double>(pageAspectRatio)))
        return;

    m_pageAspectRatio = pageAspectRatio;
    emit pageAspectRatioChanged(m_pageAspectRatio);
}

void PdfView::setReverse(bool reverse)
{
    if (m_reverse == reverse)
        return;

    m_reverse = reverse;

    emit reverseChanged(m_reverse);
}

void PdfView::setPageCheckComponent(QQmlComponent *pageCheckComponent)
{
    if (m_pageCheckComponent == pageCheckComponent)
        return;

    m_pageCheckComponent = pageCheckComponent;
    m_mapper->setShowAll(m_pageCheckComponent ? true : false);

    emit pageCheckComponentChanged(m_pageCheckComponent);
    emit maxPageIndexChanged(m_mapper->maxSpecialIndex());
    emit minPageIndexChanged(m_mapper->minSpecialIndex());
}

void PdfView::setHiddenPagesOpacity(qreal hiddenPagesOpacity)
{
    if (m_hiddenPagesOpacity == hiddenPagesOpacity)
        return;

    m_hiddenPagesOpacity = hiddenPagesOpacity;

    emit hiddenPagesOpacityChanged(m_hiddenPagesOpacity);
}

void PdfView::setPageSpacing(qreal pageSpacing)
{
    if (qFuzzyCompare(m_pageSpacing, pageSpacing))
        return;

    m_pageSpacing = pageSpacing;

    emit pageSpacingChanged(m_pageSpacing);
}

void PdfView::setFitToPage(bool fitToPage)
{
    if (m_fitToPage == fitToPage)
        return;

    m_fitToPage = fitToPage;

    emit fitToPageChanged(m_fitToPage);
}

void PdfView::setCentering(bool centering)
{
    if (m_centering == centering)
        return;

    m_centering = centering;

    emit centeringChanged(centering);
}

void PdfView::_updateContentSize()
{
    if (!qFuzzyCompare(double(m_contentWidth), double(m_mapper->contentWidth())) && m_mapper->contentWidth() > 0.0f) {
        m_contentWidth = m_mapper->contentWidth();
        emit contentWidthChanged(m_contentWidth);
    }

    if (!qFuzzyCompare(double(m_contentHeight), double(m_mapper->contentHeight())) && m_mapper->contentHeight() > 0.0f) {
        m_contentHeight = m_mapper->contentHeight();
        emit contentHeightChanged(m_contentHeight);
    }

    emit lastPageIndentChanged(lastPageIndent());
    _restoreAbsolutePosition();
}

void PdfView::_positionPages()
{
    auto paintStart = m_mapper->spacing();

    if (m_orientation == Qt::Horizontal && m_contentWidth < width())
        paintStart = (width() - m_paintedItemsSize) / 2.0f;

    auto maxSize = -1.0f;
    for (auto pageIndex : m_paintedPages) {
        if (!m_pages.contains(pageIndex))
            continue;

        if (m_mapper->isHasSpecial() && !m_mapper->isIndexSpecal(pageIndex)) {
            continue;
        }

        auto page = m_pages.value(pageIndex);
        if (page == nullptr)
            continue;

        if (!page->isVisible())
            page->setVisible(true);

        auto pagePosition = m_mapper->actualPagePosition(pageIndex);

        if (m_orientation == Qt::Vertical) {
            page->setY(paintStart - (qMax(0.0f, float(m_contentY)) - pagePosition.start * m_itemScale));

            if (page->width() <= width())
                page->setX((width() - page->width()) / 2.0f);
            else
                page->setX(-m_contentX);

            auto pageContainer = qobject_cast<PdfPageContainer *>(page);
            pageContainer->setVisibleArea(QRectF(m_contentX, m_contentY, width(), height()));

            maxSize = qMax(maxSize, float(page->width()));

            if (!qFuzzyCompare(float(m_contentTopMargin), paintStart)) {
                m_contentTopMargin = paintStart;
                emit contentTopMarginChanged(m_contentTopMargin);
            }
        } else {
            page->setX(paintStart - (qMax(0.0f, float(m_contentX)) - pagePosition.start * m_itemScale));

            if (page->height() <= height())
                page->setY((height() - page->height()) / 2.0f);
            else
                page->setY(-m_contentY);

            auto pageContainer = qobject_cast<PdfPageContainer *>(page);
            pageContainer->setVisibleArea(QRectF(m_contentX, m_contentY, width(), height()));

            maxSize = qMax(maxSize, float(page->height()));

            if (!qFuzzyCompare(m_contentTopMargin, page->y())) {
                m_contentTopMargin = page->y();
                emit contentTopMarginChanged(m_contentTopMargin);
            }
        }

        if (m_pageNumberComponent && m_pageNumberItems.keys().contains(pageIndex)) {
            auto pageNumberItem = m_pageNumberItems.value(pageIndex);
            pageNumberItem->setX((page->x() + page->width() / 2.0f) - pageNumberItem->width() / 2.0);
            pageNumberItem->setY(page->y() - pageNumberItem->property("bottomMargin").toInt() * m_itemScale);
            pageNumberItem->setScale(m_itemScale);
        }
    }

    _updateCurrentIndex();
    _updateAbsolutePosition();

    if (m_orientation == Qt::Vertical) {
        if (qAbs(maxSize - m_contentWidth) < 0.1f)
            return;

        m_contentWidth = maxSize;
        emit contentWidthChanged(m_contentWidth);
    } else {
        if (qAbs(maxSize - m_contentHeight) < 0.1f)
            return;

        m_contentHeight = maxSize;
        emit contentHeightChanged(m_contentHeight);
    }
}

void PdfView::_updateAbsolutePosition()
{
    m_absolutePosition.setX(m_contentX / m_contentWidth);
    m_absolutePosition.setY(m_contentY / m_contentHeight);
}

void PdfView::_restoreAbsolutePosition()
{
    m_contentX = m_contentWidth * m_absolutePosition.x();
    m_contentY = m_contentHeight * m_absolutePosition.y();

    emit contentXChanged(m_contentX);
    emit contentYChanged(m_contentY);
}

void PdfView::_calculateVisible()
{
    if (m_count < 0)
        return;

    auto minPageIndex = m_reverse
                            ? (m_mapper->isHasSpecial() ? m_mapper->maxSpecialIndex() : m_count - 1)
                            : (m_mapper->isHasSpecial() ? m_mapper->minSpecialIndex() : 0);

    auto startVisibleArea = qMax(0.0f, float(m_orientation == Qt::Vertical ? m_contentY : m_contentX));
    auto endVisibleArea = startVisibleArea + (m_orientation == Qt::Vertical ? height() : width());
    auto startPaintedPage = -1;
    auto endPaintedPage = -1;
    auto maxPageIndex = m_reverse ? (m_mapper->isHasSpecial() ? m_mapper->minSpecialIndex() : 0)
                                  : (m_mapper->isHasSpecial() ? m_mapper->maxSpecialIndex()
                                                              : m_count - 1);
    if (m_moveDirection <= 0) {
        if (!m_paintedPages.isEmpty())
            minPageIndex = m_reverse ? qMin(m_count - 1,
                                            *std::max_element(m_paintedPages.begin(),
                                                              m_paintedPages.end()))
                                     : qMax(0,
                                            *std::min_element(m_paintedPages.begin(),
                                                              m_paintedPages.end()));

        for (int i = minPageIndex; (m_reverse ? i >= 0 : i < m_count); (m_reverse ? --i : ++i)) {
            if (m_mapper->isHasSpecial() && !m_mapper->isIndexSpecal(i)) {
                continue;
            }

            auto pageCoordinates = m_mapper->actualPagePosition(i);

            if (pageCoordinates.end * m_itemScale >= startVisibleArea && pageCoordinates.start * m_itemScale <= startVisibleArea) {
                startPaintedPage = i;
                break;
            }

            if (pageCoordinates.start * m_itemScale >= startVisibleArea && pageCoordinates.end * m_itemScale <= endVisibleArea) {
                startPaintedPage = i;
                break;
            }
        }
    } else {
        if (!m_paintedPages.isEmpty())
            minPageIndex = m_reverse ? qMin(maxPageIndex,
                                            *std::min_element(m_paintedPages.begin(),
                                                              m_paintedPages.end()))
                                     : qMax(maxPageIndex,
                                            *std::max_element(m_paintedPages.begin(),
                                                              m_paintedPages.end()));

        for (int i = minPageIndex; (m_reverse ? i < m_count : i >= 0); (m_reverse ? ++i : --i)) {
            if (m_mapper->isHasSpecial() && !m_mapper->isIndexSpecal(i)) {
                continue;
            }

            auto pageCoordinates = m_mapper->actualPagePosition(i);

            if (pageCoordinates.end * m_itemScale >= startVisibleArea && pageCoordinates.start * m_itemScale <= startVisibleArea) {
                startPaintedPage = i;
                break;
            }
        }
    }

    for (int i = startPaintedPage; (m_reverse ? i >= 0 : i < m_count); (m_reverse ? --i : ++i)) {
        if (m_mapper->isHasSpecial() && !m_mapper->isIndexSpecal(i)) {
            continue;
        }

        auto pageCoordinate = m_mapper->actualPagePosition(i);
        if (pageCoordinate.start * m_itemScale > startVisibleArea && pageCoordinate.end * m_itemScale <= endVisibleArea)
            endPaintedPage = i;

        if (pageCoordinate.end * m_itemScale >= endVisibleArea) {
            endPaintedPage = i;
            break;
        }
    }

    if (!m_reverse) {
        if (startPaintedPage == maxPageIndex)
            endPaintedPage = startPaintedPage;

        if (startPaintedPage < 0)
            startPaintedPage = minPageIndex;
    }

    endPaintedPage = m_reverse ? qMax(endPaintedPage + 1, maxPageIndex)
                               : qMin(endPaintedPage + 1, maxPageIndex);

    m_paintedPages.clear();
    for (int i = qMax(0, (m_reverse ? endPaintedPage - 1 : startPaintedPage - 1));
         i <= (m_reverse ? startPaintedPage : endPaintedPage);
         ++i) {
        if (m_mapper->isHasSpecial() && !m_mapper->isIndexSpecal(i))
            continue;

        m_paintedPages.append(i);
    }

    if (m_reverse)
        std::sort(m_paintedPages.rbegin(), m_paintedPages.rend());
    else
        std::sort(m_paintedPages.begin(), m_paintedPages.end());

    if (!m_pageCheckComponent) {
        QMutableHashIterator<int, QQuickItem *> pagesCheckerIt(m_pageCheckerItems);
        while (pagesCheckerIt.hasNext()) {
            pagesCheckerIt.next();

            if (!pagesCheckerIt.value()) {
                pagesCheckerIt.remove();
                continue;
            }
            pagesCheckerIt.value()->deleteLater();
            pagesCheckerIt.remove();
        }
    }

    QMutableHashIterator<int, QQuickItem *> pagesIt(m_pages);
    while (pagesIt.hasNext()) {
        pagesIt.next();

        if (!pagesIt.value()) {
            pagesIt.remove();
            continue;
        }

        if (!m_paintedPages.contains(pagesIt.key())) {
            pagesIt.value()->deleteLater();
            pagesIt.remove();

            // just erase because page is checker's parent
            auto checkerIt = m_pageCheckerItems.find(pagesIt.key());
            if (checkerIt != m_pageCheckerItems.end())
                m_pageCheckerItems.erase(checkerIt);
        }
    }

    QMutableHashIterator<int, QQuickItem *> pagesNumberIt(m_pageNumberItems);
    while (pagesNumberIt.hasNext()) {
        pagesNumberIt.next();

        if (!m_paintedPages.contains(pagesNumberIt.key())) {
            pagesNumberIt.value()->deleteLater();
            pagesNumberIt.remove();
        }
    }

    m_paintedItemsSize = 0;
    for (auto pageIndex : m_paintedPages) {
        if (m_mapper->isHasSpecial() && !m_mapper->isIndexSpecal(pageIndex))
            continue;

        auto pageGeometry = m_mapper->actualPagePosition(pageIndex);
        m_paintedItemsSize += (pageGeometry.end - pageGeometry.start) * m_itemScale;
    }

    emit loadPages();
}

void PdfView::_updateCurrentIndex()
{
    auto newCurrentIndex = m_currentIndex;
    for(auto pageIndex : m_paintedPages) {
        if (!m_pages.contains(pageIndex))
            continue;

        if (m_mapper->isHasSpecial() && !m_mapper->isIndexSpecal(pageIndex)) {
            continue;
        }

        auto page = m_pages.value(pageIndex);
        if (page == nullptr)
            continue;

        if (m_orientation == Qt::Vertical) {
            if (m_catchBound >= page->y() && m_catchBound < page->y() + page->height()) {
                if (m_currentIndex != pageIndex) {
                    newCurrentIndex = pageIndex;
                }
            }
        } else {
            if (m_catchBound >= page->x() && m_catchBound < page->x() + page->width()) {
                if (m_currentIndex != pageIndex)
                    newCurrentIndex = pageIndex;
            }
        }
    }

    if (newCurrentIndex != m_currentIndex) {
        m_currentIndex = newCurrentIndex;
        emit currentIndexChanged(m_currentIndex);
    }
}

void PdfView::_processActivatedAnnotation(BaseAnnotation *annotation)
{
    if (annotation == nullptr)
        return;

    if (annotation->type == BaseAnnotation::AnnotationType::Url) {
        if (!annotation->content.isEmpty())
            emit clickedUrl(annotation->content);

        return;
    }

    if (annotation->type != BaseAnnotation::AnnotationType::Link)
        return;

    auto pageIndex = annotation->linkToPage;
    if (pageIndex < 0 && pageIndex >= m_count)
        return;

    auto pageGeometry = m_mapper->originalPageGeometry(pageIndex);
    auto pageCoordinate = annotation->pageCoordinate;

    if (pageCoordinate.y() <= 0)
        pageCoordinate.setY(0);

    if (pageCoordinate.x() <= 0)
        pageCoordinate.setX(0);

    auto targetPageSize = PdfPageContainer::pageCurrentSize(pageGeometry,
                                                                 {width(), height()},
                                                                 m_orientation,
                                                                 m_itemScale);
    auto pageRate = targetPageSize.width() / pageGeometry.width();
    QPointF linkOnPagePosition(pageCoordinate.x() * pageRate,
                               (pageGeometry.height() - pageCoordinate.y()) * pageRate);

    auto pagePosition = m_mapper->actualPagePosition(pageIndex);
    if (m_orientation == Qt::Vertical) {
        if (pageCoordinate.y() <= 0) {
            positionViewAtIndex(pageIndex);
        } else {
            setContentY((pagePosition.start + (pageGeometry.height() - pageCoordinate.y()) * pageRate) * m_itemScale);
        }
    } else {
        setContentX(pagePosition.start * m_itemScale);
    }

    _positionPages();

    emit clickedGoToPage(pageIndex, pageCoordinate);
}

void PdfView::_loadDocument()
{
    if (m_pdfPath.isEmpty())
        return;

    if (m_documentProvider == nullptr)
        return;

    emit statusChanged(BasePdfDocument::DocumentStatus::Loading);

    m_documentProvider->setPath(m_pdfPath);
}

void PdfView::_documentEdited()
{
    m_documentEdited = true;
    emit documentEditedChanged(true);
}

void PdfView::_pageChecked(int index, bool checked)
{
    if (index < 0 || index > m_count)
        return;

    bool isIndexSpecal = m_mapper->isIndexSpecal(index);
    if (isIndexSpecal) {
        if (!checked) {
            m_mapper->removeFromSpecial(index);
            emit _specialIndexesChanged();
        }
    } else {
        if (checked) {
            m_mapper->insertToSpecial(index);
            emit _specialIndexesChanged();
        }
    }
}

void PdfView::_updateSpecailIndexes()
{
    if (!m_pageCheckComponent)
        return;

    QString pageRange;
    for (const auto &range : m_mapper->specialRangeIndexes()) {
        pageRange += ((pageRange.isEmpty() ? QStringLiteral() : QStringLiteral(","))
                      + (QString::number(range.first + 1) + QStringLiteral("-")
                         + QString::number(range.second + 1)));
    }

    QString pageIndexes;
    for (const auto &index : m_mapper->specialIndexes()) {
        pageIndexes += (((pageRange.endsWith(",") || (pageRange.isEmpty() && pageIndexes.isEmpty()))
                             ? QStringLiteral()
                             : QStringLiteral(","))
                        + QString::number(index + 1));
    }

    m_pageRange = pageRange + pageIndexes;
    emit pageRangeChanged(m_pageRange);
    emit maxPageIndexChanged(m_mapper->maxSpecialIndex());
    emit minPageIndexChanged(m_mapper->minSpecialIndex());
}

void PdfView::_updateSpecialPagesCount(int specialPagesCount)
{
    if (m_specialPagesCount == specialPagesCount)
        return;

    m_specialPagesCount = specialPagesCount;
    emit specialPagesCountChanged(m_specialPagesCount);
}

void PdfView::_preparePages()
{
    if (m_paintedPages.isEmpty())
        return;

    auto baseDocument = qobject_cast<BasePdfDocument *>(m_documentProvider);

    bool needPositioning = false;
    for (const auto &pageIndex : m_paintedPages) {
        if (m_mapper->isHasSpecial() && !m_mapper->isIndexSpecal(pageIndex)) {
            continue;
        }

        if (m_pages.keys().contains(pageIndex)) {
            auto pageContainer = qobject_cast<PdfPageContainer *>(m_pages.value(pageIndex));
            if (!pageContainer->source()) {
                auto pageSource = m_documentProvider->loadPage(pageContainer->index());
                if (pageSource) {
                    pageContainer->setPageSource(pageSource);
                    pageContainer->setCentering(m_centering);
                    pageContainer->setFitToPage(m_fitToPage);
                    needPositioning = true;
                }
            }

            _preparePageChecker(pageContainer, pageIndex);

            continue;
        }

        auto page = new PdfPageContainer(this);
        page->setCentering(m_centering);
        page->setFitToPage(m_fitToPage);

        connect(page, &PdfPageContainer::pageReady, this, &PdfView::_positionPages);
        connect(this, &PdfView::orientationChanged, page, &PdfPageContainer::setOrientation);
        connect(this, &PdfView::itemScaleChanged, page, &PdfPageContainer::setScale);
        connect(this, &PdfView::annotationsPaintChanged, page, &PdfPageContainer::setAnnotationsPaint);
        connect(page, &PdfPageContainer::annotationActivate, this, &PdfView::_processActivatedAnnotation);
        connect(this, &PdfView::notesPaintChanged, page, &PdfPageContainer::setNotesPaint);
        connect(page, &PdfPageContainer::noteActivate, this, &PdfView::noteActivated);
        connect(page, &PdfPageContainer::pageChanged, this, &PdfView::_documentEdited);
        connect(page, &PdfPageContainer::noteRemoved, this, [pageIndex, this](int noteId, bool result) {
            emit noteRemoved(pageIndex, noteId, result);
        });
        connect(page, &PdfPageContainer::noteEdited, this, [pageIndex, this](int noteId, bool result) {
            emit noteEdited(pageIndex, noteId, result);
        });
        connect(this, &PdfView::grayScaleRenderingChanged, page, &PdfPageContainer::setGrayScaleRendering);
        connect(page, &PdfPageContainer::sendCheckChangedInfo, this, &PdfView::_pageChecked);

        page->setVisible(false);
        page->setPageGeometry(m_mapper->originalPageGeometry(pageIndex));
        page->setOrientation(m_orientation);
        page->setRequestedSize({ width(), height() });
        page->setScale(m_itemScale);
        page->setMapper(m_mapper);
        page->setAnnotationsPaint(m_annotationsPaint);
        page->setNotesPaint(m_notesPaint);
        page->setGrayScaleRendering(m_grayScaleRendering);

        auto pageSource = baseDocument->loadPage(pageIndex);
        if (pageSource)
            page->setPageSource(pageSource);

        m_pages.insert(pageIndex, page);

        if (m_pageNumberComponent && !m_pageNumberItems.keys().contains(pageIndex)) {
            QString pageNumber = QString::number(pageIndex + 1);
            QQuickItem *pageNumberItem = qobject_cast<QQuickItem*>(m_pageNumberComponent->create());
            if (pageNumberItem) {
                pageNumberItem->setProperty("currentPageNumber", pageNumber);
                pageNumberItem->setScale(m_itemScale);
                pageNumberItem->setVisible(true);
                pageNumberItem->setParentItem(this);
                m_pageNumberItems.insert(pageIndex, pageNumberItem);
            }
        }

        _preparePageChecker(page, pageIndex);

        needPositioning = true;
    }

    if (needPositioning)
        _positionPages();
}

void PdfView::_preparePageChecker(PdfPageContainer *page, int pageIndex)
{
    if (!m_pageCheckComponent)
        return;

    auto isCheck = m_mapper->isIndexSpecal(pageIndex);
    QQuickItem *pageCheckerItem = nullptr;
    if (m_pageCheckerItems.keys().contains(pageIndex))
        pageCheckerItem = m_pageCheckerItems.value(pageIndex);
    else {
        pageCheckerItem = qobject_cast<QQuickItem *>(m_pageCheckComponent->create());
        m_pageCheckerItems.insert(pageIndex, pageCheckerItem);
    }

    if (pageCheckerItem) {
        pageCheckerItem->setParentItem(page);
        pageCheckerItem->setVisible(true);
    }
    page->setOpacity(isCheck ? 1.0 : m_hiddenPagesOpacity);
    page->setIndex(pageIndex);
    page->setChecked(isCheck);
}
