// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QTimer>
#include <QSGTexture>
#include <QSGSimpleTextureNode>
#include <QPropertyAnimation>
#include <QQuickWindow>
#include <QThreadPool>

#include "basepage.h"
#include "bitmaploadworker.h"
#include "pagebitmaphelper.h"

#include "pdfpagetile.h"

PdfPageTile::PdfPageTile(QQuickItem *parent)
    : QQuickItem(parent)
    , m_needUpdateImage(false)
    , m_renderInProcess(false)
    , m_renderable(false)
    , m_imageScale(1.0)
    , m_renderFlags(0)
    , m_centering(false)
    , m_fitToPage(true)
{
    setFlag(QQuickItem::ItemHasContents, true);

    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &PdfPageTile::_loadBitmap);

    m_timer->setInterval(150);
    m_timer->setSingleShot(true);

    connect(this, &PdfPageTile::renderableChanged, m_timer, static_cast<void(QTimer::*)(void)>(&QTimer::start));
    connect(this, &PdfPageTile::bitmapError, m_timer, static_cast<void(QTimer::*)(void)>(&QTimer::start));
    connect(this, &PdfPageTile::partReady, this, &PdfPageTile::update);

    m_animation = new QPropertyAnimation(this, "opacity");
    m_animation->setDuration(180);
    m_animation->setStartValue(0.42);
    m_animation->setEndValue(1.0);

    connect(m_animation, &QPropertyAnimation::finished, this, &PdfPageTile::animationEnded);
}

PdfPageTile::~PdfPageTile()
{
    m_timer->stop();
}

QSGNode *PdfPageTile::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    if (height() <= 0 || width() <= 0)
        return nullptr;

    auto node = static_cast<QSGSimpleTextureNode *>(oldNode);

    if (node == nullptr) {
        node = new QSGSimpleTextureNode();
        node->setOwnsTexture(true);
    }

    if (node == nullptr)
        qFatal("Error: create node error.");

    if (node->texture() == nullptr) {
        auto paper = QImage(1, 1, QImage::Format_RGBA8888);
        paper.fill(Qt::transparent);

        node->setTexture(window()->createTextureFromImage(paper, QQuickWindow::TextureHasAlphaChannel));

        if (!m_pagePart.isNull())
            m_needUpdateImage = true;
    }

    if (m_renderable && m_needUpdateImage) {
        if (m_pagePart.isNull()) {
            emit bitmapError();
        } else {
            node->texture()->deleteLater();
            node->setTexture(window()->createTextureFromImage(m_pagePart));
        }

        m_needUpdateImage = false;
    }

    if (!m_renderInProcess && m_pagePart.isNull())
        emit bitmapError();

    node->setRect(boundingRect());

    return node;
}

void PdfPageTile::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    QQuickItem::geometryChanged(newGeometry, oldGeometry);
    m_timer->start();
}

void PdfPageTile::setPageSource(QSharedPointer<BasePage> pageSource)
{
    if (!pageSource)
        return;

    m_pageSource = pageSource;
    m_timer->start();
}

void PdfPageTile::setActualPageSize(const QSizeF &actualPageSize)
{
    if (m_actualPageSize == actualPageSize)
        return;

    m_actualPageSize = actualPageSize;
}

bool PdfPageTile::renderable() const
{
    return m_renderable;
}

bool PdfPageTile::isBitmap() const
{
    return !m_pagePart.isNull();
}

void PdfPageTile::setImageScale(qreal imageScale)
{
    if (qFuzzyCompare(imageScale, m_imageScale))
        return;

    m_imageScale = imageScale;
    m_timer->start();
}

void PdfPageTile::render()
{
    m_timer->start();
}

void PdfPageTile::setRenderable(bool renderable)
{
    if (m_renderable == renderable)
        return;

    m_renderable = renderable;
    emit renderableChanged(m_renderable);
}

void PdfPageTile::setRenderFlags(int renderFlags)
{
    if (m_renderFlags == renderFlags)
        return;

    m_renderFlags = renderFlags;
    m_timer->start();
}

void PdfPageTile::setCentering(bool centering)
{
    if (m_centering == centering)
        return;

    m_centering = centering;
}

void PdfPageTile::setFitToPage(bool fitToPage)
{
    if (m_fitToPage == fitToPage)
        return;

    m_fitToPage = fitToPage;
}

void PdfPageTile::_loadBitmap()
{
    if (!m_renderable)
        return;

    if (!m_pageSource)
        return;

    if (m_renderInProcess)
        return;

    if (width() <= 0 || height() <= 0)
        return;

    auto pageSize = m_pageSource->originalSize();
    if (!pageSize.isFinished() || pageSize.isCanceled()) {
        m_timer->start();
        return;
    }

    auto pageScaleX = width() / pageSize.result().width();
    auto pageScaleY = height() / pageSize.result().height();

    auto *bitmapLoader = new BitmapLoaderWorker(m_pageSource,
                                                pageScaleX,
                                                pageScaleY,
                                                m_renderFlags,
                                                m_imageScale,
                                                bitmaphelper::bias(QPointF(-x(), -y()),
                                                                   QSizeF(pageSize.result().width(),
                                                                          pageSize.result().height()),
                                                                   m_actualPageSize,
                                                                   m_imageScale,
                                                                   m_centering,
                                                                   m_fitToPage));
    connect(this, &PdfPageTile::destroyed, bitmapLoader, &BitmapLoaderWorker::cancel);
    connect(bitmapLoader, &BitmapLoaderWorker::done, this, [this](QImage result) {
        m_renderInProcess = false;
        if (result.isNull()) {
            emit bitmapError();
        } else {
            auto needAnimation = m_pagePart.isNull();
            m_pagePart = result;
            m_needUpdateImage = true;
            emit partReady();

            if (needAnimation) {
                m_animation->setStartValue(m_pagePart.isNull() ? 0.42 : 0.95);
                m_animation->start();
            }
            update();
        }
    });

    QThreadPool::globalInstance()->start(bitmapLoader);
    m_renderInProcess = true;
}
