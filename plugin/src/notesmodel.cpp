// SPDX-FileCopyrightText: 2022 - 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "interface/baseannotation.h"
#include "interface/basepage.h"

#include "notesmodel.h"

NotesModel::NotesModel(QObject *parent) : QAbstractListModel(parent),
    m_loading(true)
{ }

int NotesModel::rowCount(const QModelIndex &parent) const
{
    if (!m_pageSource)
        return 0;

    return parent.isValid() ? 0 : m_notes.size();
}

QVariant NotesModel::data(const QModelIndex &index, int role) const
{
    if (!m_pageSource)
        return {  };

    if (!index.isValid() || index.row() > m_notes.size())
        return {  };

    const auto *note = m_notes.at(index.row());
    if (note == nullptr)
        return {  };

    switch (role) {
    case ContentRole: return QVariant::fromValue(note->content);
    case AuthorRole: return QVariant::fromValue(note->author);
    case ColorRole: return QVariant::fromValue(note->color);
    case IdRole: return QVariant::fromValue(note->annotationId);
    }

    return {  };
}

QHash<int, QByteArray> NotesModel::roleNames() const
{
    return {
        { ContentRole, "content" },
        { AuthorRole, "author" },
        { ColorRole, "color" },
        { IdRole, "id" }
    };
}

void NotesModel::setPageSource(QSharedPointer<BasePage> pageSource)
{
    if (!pageSource || pageSource == m_pageSource)
        return;

    m_pageSource = pageSource;
    connect(m_pageSource.data(), &BasePage::annotationsLoaded, this, &NotesModel::_collectNotes);
    m_pageSource->loadAnnotations();
    m_loading = true;
    emit loadingChanged(m_loading);

    _collectNotes();
}

bool NotesModel::isLoading() const
{
    return m_loading;
}

void NotesModel::_collectNotes()
{
    beginResetModel();

    m_notes.clear();
    const auto annotations = m_pageSource->annotations();
    for (const auto &annotation : annotations) {
        if (annotation == nullptr)
            continue;

        if (annotation->type == BaseAnnotation::AnnotationType::HighLight
                || annotation->type == BaseAnnotation::AnnotationType::Text) {
            m_notes.append(annotation);

            continue;
        }
    }

    endResetModel();
    m_loading = false;
    emit loadingChanged(m_loading);
}

