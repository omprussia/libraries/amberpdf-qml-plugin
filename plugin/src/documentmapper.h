// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef DOCUMENTMAPPER_H
#define DOCUMENTMAPPER_H

#include <QQuickItem>
#include <QSizeF>
#include <QPointF>
#include <QHash>
#include <set>
#include <map>

#include "pdfviewtypes.h"

#define SPACING_DEFAULT_VALUE 20

class BasePdfDocument;
class DocumentMapper : public QQuickItem
{
    Q_OBJECT

public:
    explicit DocumentMapper(QQuickItem *parent = nullptr);

    QSGNode *updatePaintNode(QSGNode *, UpdatePaintNodeData *) override;

    QHash<int, PagePosition> actualMap() const;
    PagePosition actualPagePosition(int pageIndex) const;
    PageGeometry originalPageGeometry(int pageIndex) const;
    QPointF pagePosition(int pageIndex) const;
    qreal contentHeight() const;
    qreal contentWidth() const;
    qreal spacing() const;
    qreal lastPageActualSize() const;
    void forceUpdate();
    bool isIndexSpecal(int pageIndex) const;
    bool isHasSpecial() const;
    int minSpecialIndex() const;
    int maxSpecialIndex() const;
    void removeFromSpecial(int index);
    void insertToSpecial(int index);
    qreal pageAspectRatio() const;
    std::set<int> specialIndexes() const;
    std::map<int, int> specialRangeIndexes() const;
    int specialPagesCount() const;
    bool fitToPage() const;

public slots:
    void setDocumentProvider(BasePdfDocument *documentProvider);
    void setOrientation(Qt::Orientation orientation);
    void setSpecialPageIndexes(const std::map<int,int> &specialRangeIndexes,
 const std::set<int> &specialIndexes);
    void setPageAspectRatio(qreal pageAspectRatio);
    void setReverse(bool reverse);
    void setShowAll(bool showAll);
    void setSpacing(qreal spacing);
    void setFitToPage(bool fitToPage);

signals:
    void contentWidthChanged(qreal);
    void contentHeightChanged(qreal);
    void mapEnd();
    void specialPagesCountChanged(int specialPagesCount);

private slots:
    void _mapPages();
    void _updateSize();

private:
    BasePdfDocument *m_documentProvider;
    PagesMap m_originalPagesMap;
    QHash<int, PagePosition> m_actualPagesCoordinates;
    Qt::Orientation m_orientation;
    qreal m_spacing;
    qreal m_contentHeight;
    qreal m_contentWidth;
    qreal m_lastPageActualSize;
    std::set<int> m_specialIndexes;
    std::map<int, int> m_specialRangeIndexes;
    qreal m_pageAspectRatio;
    bool m_reverse;
    bool m_showAll;
    int m_specialPagesCount;
    bool m_fitToPage;
};

#endif // DOCUMENTMAPPER_H
