// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PAGEWITHNOTESSORTMODEL_H
#define PAGEWITHNOTESSORTMODEL_H

#include <QSortFilterProxyModel>

class PagesWithNotesModel;
class PageWithNotesSortModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(PagesWithNotesModel *sourceModel READ sourceModel WRITE setSourceModel NOTIFY sourceModelChanged)
    Q_PROPERTY(int filterParameter READ filterParameter WRITE setFilterParameter NOTIFY filterParameterChanged)

public:
    enum FilterParameter {
        None,
        Count
    };
    Q_ENUM(FilterParameter)

    explicit PageWithNotesSortModel(QObject *parent = nullptr);
    PagesWithNotesModel * sourceModel() const;
    int filterParameter() const;

    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

public slots:
    void setSourceModel(PagesWithNotesModel *sourceModel);
    void setFilterParameter(int filterParametr);

signals:
    void sourceModelChanged(PagesWithNotesModel * sourceModel);
    void filterParameterChanged(int filterParametr);

private:
    PagesWithNotesModel * m_sourceModel;
    int m_filterParameter;
};

#endif // PAGEWITHNOTESSORTMODEL_H
