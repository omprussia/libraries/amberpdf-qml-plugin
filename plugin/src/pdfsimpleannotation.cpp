// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include <QQuickWindow>
#include <QSGSimpleTextureNode>

#include "baseannotation.h"

#include "pdfsimpleannotation.h"

PdfSimpleAnnotation::PdfSimpleAnnotation(QQuickItem *parent, BaseAnnotation *source) : QQuickItem(parent),
    m_annotationSource(source),
    m_highlighted(false),
    m_needUpdateImage(false)
{
    setFlag(QQuickItem::ItemHasContents, true);
    setAcceptedMouseButtons(Qt::AllButtons);

    connect(this, &PdfSimpleAnnotation::xChanged, this, &PdfSimpleAnnotation::update);
    connect(this, &PdfSimpleAnnotation::yChanged, this, &PdfSimpleAnnotation::update);
    connect(this, &PdfSimpleAnnotation::widthChanged, this, &PdfSimpleAnnotation::update);
    connect(this, &PdfSimpleAnnotation::heightChanged, this, &PdfSimpleAnnotation::update);

    connect(parent, &QQuickItem::yChanged, this, &PdfSimpleAnnotation::clearHighlight);
    connect(parent, &QQuickItem::xChanged, this, &PdfSimpleAnnotation::clearHighlight);

    m_paper = QImage(1, 1, QImage::Format_RGBA8888);
    m_paper.fill(qRgba(1, 222, 121, HIGHLIGHT_OPACITY_ANNOT_OFF));
}

QSGNode *PdfSimpleAnnotation::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    if (height() <= 0 || width() <= 0)
        return nullptr;

    auto node = static_cast<QSGSimpleTextureNode *>(oldNode);

    if (node == nullptr) {
        node = new QSGSimpleTextureNode();
        node->setOwnsTexture(true);
    }

    if (m_needUpdateImage || node->texture() == nullptr) {
        if (node->texture() != nullptr)
            node->texture()->deleteLater();

        node->setTexture(window()->createTextureFromImage(m_paper));

        m_needUpdateImage = false;
    }

    node->setRect(boundingRect());

    return node;
}

bool PdfSimpleAnnotation::event(QEvent *event)
{
    if (opacity() < 1.0f)
        return QQuickItem::event(event);

    switch (event->type()) {
    case QEvent::MouseButtonPress:
        _setHighlight(true);
        return true;

    case QEvent::MouseButtonRelease:
        if (m_annotationSource != nullptr)
            emit triggered(m_annotationSource);
        _setHighlight(false);
        break;

    case QEvent::MouseMove:
        _setHighlight(false);
        break;

    default:
        break;
    }

    return QQuickItem::event(event);
}

void PdfSimpleAnnotation::_setHighlight(bool highlight)
{
    if (m_highlighted != highlight) {
        m_highlighted = highlight;
        m_needUpdateImage = true;
        m_paper.fill(qRgba(1, 222, 121, highlight ? HIGHLIGHT_OPACITY_ANNOT_ON : HIGHLIGHT_OPACITY_ANNOT_OFF));
        update();
    }
}

void PdfSimpleAnnotation::clearHighlight()
{
    _setHighlight(false);
}

BaseAnnotation *PdfSimpleAnnotation::source() const
{
    return m_annotationSource;
}
