// SPDX-FileCopyrightText: 2022-2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef PDFPAGETILE_H
#define PDFPAGETILE_H

#include <QQuickItem>
#include <QImage>

class BasePage;
class QTimer;
class QSGTexture;
class QPropertyAnimation;
class PdfPageTile : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(bool renderable READ renderable WRITE setRenderable NOTIFY renderableChanged)

public:
    explicit PdfPageTile(QQuickItem *parent = nullptr);
     ~PdfPageTile() override;

    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry) override;

    void setPageSource(QSharedPointer<BasePage> pageSource);
    void setActualPageSize(const QSizeF &actualPageSize);
    bool renderable() const;
    bool isBitmap() const;

public slots:
    void setImageScale(qreal imageScale);
    void render();
    void setRenderable(bool renderable);
    void setRenderFlags(int renderFlags);
    void setCentering(bool centering);
    void setFitToPage(bool fitToPage);

signals:
    void bitmapError();
    void partReady();
    void animationEnded();
    void renderableChanged(bool renderable);

private slots:
    void _loadBitmap();

private:
    QImage m_pagePart;
    QSharedPointer<BasePage> m_pageSource;
    bool m_needUpdateImage;
    bool m_renderInProcess;
    bool m_renderable;
    QTimer *m_timer;
    qreal m_imageScale;
    QPropertyAnimation *m_animation;
    int m_renderFlags;
    bool m_centering;
    bool m_fitToPage;
    QSizeF m_actualPageSize;
};

#endif // PDFPAGETILE_H
