// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef NOTESMODEL_H
#define NOTESMODEL_H

#include <QAbstractItemModel>
#include <QSharedPointer>

class BasePage;
class BaseAnnotation;
class NotesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum NoteRoles
    {
        ContentRole = Qt::UserRole + 1,
        AuthorRole,
        ColorRole,
        IdRole
    };

    explicit NotesModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    void setPageSource(QSharedPointer<BasePage> pageSource);
    bool isLoading() const;

signals:
    void loadingChanged(bool);

private slots:
    void _collectNotes();

private:
    QSharedPointer<BasePage> m_pageSource;
    QList<BaseAnnotation *> m_notes;
    bool m_loading;
};


#endif // NOTESMODEL_H
