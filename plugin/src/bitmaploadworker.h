// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef BITMAPLOADWORKER_H
#define BITMAPLOADWORKER_H

#include <QObject>
#include <QRunnable>
#include <QSharedPointer>
#include <QImage>
#include <QFutureWatcher>
#include <QMutex>

class BasePage;
class BitmapLoaderWorker : public QObject, public QRunnable
{
    Q_OBJECT

public:
    BitmapLoaderWorker(QSharedPointer<BasePage> p, qreal scaleX, qreal scaleY, int flags, qreal zoom = 1.0, QPointF bias = QPointF());
    ~BitmapLoaderWorker() override;

    void run() override;

public slots:
    void cancel();

signals:
    void done(QImage);

private slots:
    void _getResult();

private:
    QSharedPointer<BasePage> m_page;
    QFutureWatcher<QImage> m_watcher;
    qreal m_scaleX;
    qreal m_scaleY;
    int m_flags;
    qreal m_zoom;
    QPointF m_bias;
    static QMutex m_mutex;
    bool m_isCanceled;
};

#endif // BITMAPLOADWORKER_H
