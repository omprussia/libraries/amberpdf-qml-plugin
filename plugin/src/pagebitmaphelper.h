// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#pragma once

#include <QPointF>
#include <QSizeF>

namespace bitmaphelper {

QPointF bias(const QPointF &startPosition,
             const QSizeF &originalPageSize,
             const QSizeF &actualPageSize,
             qreal imageScale,
             bool centering,
             bool fitToPage);

}
