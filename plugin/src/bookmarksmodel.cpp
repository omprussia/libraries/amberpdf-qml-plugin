// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "interface/basebookmark.h"
#include "bookmarksmodel.h"

BookmarksModel::BookmarksModel(QObject *parent) : QAbstractListModel(parent) {  }

int BookmarksModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : m_data.size();
}

QVariant BookmarksModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= m_data.size())
        return {  };

    const auto *bookmark = m_data.at(index.row());
    if (bookmark == nullptr)
        return {  };

    switch (role) {
    case LevelRole : return QVariant::fromValue(bookmark->level);
    case TitleRole: return QVariant::fromValue(bookmark->title);
    case PageIndexRole: return QVariant::fromValue(bookmark->pageIndex);
    case InPageXRole: return QVariant::fromValue(bookmark->positionInPage.x());
    case InPageYRole: return QVariant::fromValue(bookmark->positionInPage.y());
    }

    return {  };
}

QHash<int, QByteArray> BookmarksModel::roleNames() const
{
    return {
        { LevelRole, "level" },
        { TitleRole, "title" },
        { PageIndexRole, "pageIndex" },
        { InPageXRole, "pageX" },
        { InPageYRole, "pageY" }
    };
}

void BookmarksModel::setNewData(const QVector<BaseBookmark *> &newData)
{
    beginResetModel();
    m_data.clear();
    m_data = newData;
    endResetModel();
}
