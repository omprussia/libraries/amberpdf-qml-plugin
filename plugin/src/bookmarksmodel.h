// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef BOOKMARKSMODEL_H
#define BOOKMARKSMODEL_H

#include <QAbstractListModel>
#include <QVector>

struct BaseBookmark;
class BookmarksModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum FilePropertyRoles
    {
        LevelRole = Qt::UserRole + 1,
        TitleRole,
        PageIndexRole,
        InPageXRole,
        InPageYRole
    };

    explicit BookmarksModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    void setNewData(const QVector<BaseBookmark *> &newData);

private:
    QVector<BaseBookmark *> m_data;
};

#endif // BOOKMARKSMODEL_H
