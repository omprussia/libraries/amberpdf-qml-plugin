// SPDX-FileCopyrightText: 2022 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QMutexLocker>

#include "basepage.h"

#include "bitmaploadworker.h"

QMutex BitmapLoaderWorker::m_mutex;

BitmapLoaderWorker::BitmapLoaderWorker(QSharedPointer<BasePage> p, qreal scaleX, qreal scaleY, int flags, qreal zoom , QPointF bias):
    m_page(p),
    m_scaleX(scaleX),
    m_scaleY(scaleY),
    m_flags(flags),
    m_zoom(zoom),
    m_bias(bias),
    m_isCanceled(false)
{  }

BitmapLoaderWorker::~BitmapLoaderWorker()
{
    cancel();
}

void BitmapLoaderWorker::run() {
    if (!m_page) {
        emit done({  });
        return;
    }

    if (m_isCanceled) {
        emit done({  });
        return;
    }

    setAutoDelete(false);
    connect(&m_watcher, &QFutureWatcher<QImage>::finished, this, &BitmapLoaderWorker::_getResult);
    QMutexLocker lock(&m_mutex);
    m_watcher.setFuture(m_page->bitmapPart(m_scaleX, m_scaleY, m_flags, m_zoom, m_bias));
}

void BitmapLoaderWorker::cancel()
{
    if (m_watcher.isRunning())
        m_watcher.cancel();

    m_isCanceled = true;
    setAutoDelete(true);
    deleteLater();
}

void BitmapLoaderWorker::_getResult()
{
    if (m_watcher.isFinished() && !m_watcher.isCanceled())
        emit done(m_watcher.result());
    else
        emit done({  });

    setAutoDelete(true);
    deleteLater();
}
