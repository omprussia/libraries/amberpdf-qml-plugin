# Authors

* Alexey Fedchenko
  * Developer, 2020-2022
  * Maintainer, 2021-2022
* Anton Zernin
  * Reviewer, 2021-2022
  * Maintainer, 2022-2023
* Alexey Andreev
  * Reviewer, 2022-2023
* Kira Sagalakova
  * Reviewer, 2024
  * Maintainer, 2024
* Kirill Chuvilin
  * Developer, 2020
  * Maintainer, 2020
  * Product owner, 2020-2022
  * Reviewer, 2020-2021
* Liliya Nasyrova
  * QA Engineer, 2020-2022
* Tatyana Kaznova
  * Product manager, 2021-2022
* Valeriya Kuchaeva
  * UI Designer, 2020-2022
* Yulia Stepanova
  * Product manager, 2020-2021
